/*
 * main.m
 * Main code file for the libobjcsdl demo program
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <stdint.h>
#import <stdio.h>
#import <stdlib.h>
#import <time.h>
#import "../lib/objcsdl.h"

//function prototypes
int producer(void* data); //producer thread callback
int consumer(void* data); //consumer thread callback
void produce(); //called by the producer thread
void consume(); //called by the consumer thread

//global variables
SDLContext* ctxt = nil;
SDLMutex* lock = nil; //the protective mutex
SDLCondition* canProduce = nil; //can the producer thread run?
SDLCondition* canConsume = nil; //can the consumer thread run?
int modData = -1; //the data to modify

//main function - main entry point for the program
int main(int argc, const char* argv[]) {
	//create an autorelease pool
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

	//create an SDLContext instance
	ctxt = [[SDLContext alloc] init];

	//create an SDLWindow
	SDLWindow* win = [[SDLWindow alloc] 
				initWithTitle: @"Mutex and Condition Demo"
				andWidth: 640
				andHeight: 480];

	//create a renderer from the window
	SDLRenderer* rend = [[SDLRenderer alloc] initWithWindow: win];

	//get the splash image
	SDLImgElement* splash = [[SDLImgElement alloc] 
			initWithImagePath: @"src/test/assets/splash.png"];

	//get a texture from the splash image
	SDLTexture* splashTex = [[SDLTexture alloc] initWithImage: splash
					andRenderer: rend];

	//initialize the mutex
	lock = [[SDLMutex alloc] init];

	//initialize the condition objects
	canProduce = [[SDLCondition alloc] init];
	canConsume = [[SDLCondition alloc] init];

	//create two thread objects to use as
	//consumer and producer threads
	SDLThread* pThread = [[SDLThread alloc] initWithName: @"Producer"
						andString: @"Producer"
					andCallback: producer];
	SDLThread* cThread = [[SDLThread alloc] initWithName: @"Consumer"
						andString: @"Consumer"
				      andCallback: consumer];

	//create an event handler
	SDLEventHandler* handler = [[SDLEventHandler alloc] init];

	//declare a sentinel variable
	bool quit = NO;

	//run the threads
	[pThread run];
	[cThread run];

	//main loop
	while(!quit) {
		
		//event loop
		while([handler pollNextEvent] != 0) {
			//get the polled event
			SDLEvent* event = [handler currentEvent];

			//and handle it
			if([event triggeredBy: EVNT_QUIT]) {
				quit = YES;
			} 

		}

		//set the renderer's draw color to white
		[rend setDrawColor: [[SDLColor alloc] initWithRed: 0xFF
			  				andGreen: 0xFF
							 andBlue: 0xFF
							andAlpha: 0xFF]];
		
		//clear the screen
		[rend clearScreen];

		//render the splash texture
		[rend renderTexture: splashTex];

		//and update the screen
		[rend refreshScreen];
	}

	//release the event handler
	[handler release];

	//wait for the threads to finish
	[cThread wait];
	[pThread wait];

	//release the conditions
	[canConsume release];
	[canProduce release];

	//release the mutex
	[lock release];

	//release the splash image objects
	[splashTex release];
	[splash release];

	//release the renderer
	[rend release];

	//release the window
	[win release];

	//release the context
	[ctxt release];

	//drain the pool
	[pool drain];

	//and exit with no errors
	return EXIT_SUCCESS;
}

//producer function - runs the producer thread
int producer(void* data) {
	//print that the thread started
	printf("\nProducer started...\n");

	//seed the thread's RNG
	srand(time(NULL));

	//produce 5 times
	int i;
	for(i = 0; i < 5; i++) {
		//delay for a random amount of time
		[ctxt delayMilliseconds: rand() % 1000];

		//and modify the data
		produce();
	}

	//print that the thread finished
	printf("\nProducer finished!\n");

	//and return from the thread
	return 0;
}

//consumer function - runs the consumer thread
int consumer(void* data) {
	//print that the thread started
	printf("\nConsumer started...\n");

	//seed the thread's RNG
	srand(time(NULL));

	//consume 5 times
	int i;
	for(i = 0; i < 5; i++) {
		//delay for a random amount of time
		[ctxt delayMilliseconds: rand() % 1000];

		//and modify the data
		consume();
	}

	//print that the thread finished
	printf("\nConsumer finished!\n");

	//and return from the thread
	return 0;
}

//produce function - called by the producer thread
void produce() {
	//lock the mutex
	[lock lock];

	//handle modified data
	if(modData != -1) {
		//wait for the data to be cleared
		printf("\nProducer found modified data, "
			"waiting for consumer to clear the data...\n");
		[canProduce waitWithMutex: lock];
	}

	//modify the data
	modData = rand() % 255;
	printf("\nProduced %d\n", modData);

	//unlock the mutex
	[lock unlock];

	//and signal the consumer thread
	[canProduce signal: canConsume];
}

//consume function - called by the consumer thread
void consume() {
	//lock the mutex
	[lock lock];

	//handle cleared data
	if(modData == -1) {
		//wait for the data to be modified
		printf("\nConsumer found cleared data, "
			"waiting for producer to modify the data...\n");
		[canConsume waitWithMutex: lock];
	}

	//clear the data
	printf("\nConsumed %d\n", modData);
	modData = -1;

	//unlock the mutex
	[lock unlock];

	//and signal the producer thread
	[canConsume signal: canProduce];
}

//end of program
