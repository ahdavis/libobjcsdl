/*
 * objcsdl.h
 * Master import file for the objcsdl library
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import "except/SDLInitException.h"
#import "except/SDLWindowException.h"
#import "except/SDLExtException.h"
#import "except/SDLImgException.h"
#import "except/SDLOptException.h"
#import "except/SDLScaleException.h"
#import "except/SDLBlitException.h"
#import "except/SDLRendererException.h"
#import "except/SDLTextureException.h"
#import "except/SDLFontException.h"
#import "except/SDLTextException.h"
#import "except/SDLSimpleMsgBoxException.h"
#import "except/SDLSoundException.h"
#import "except/SDLTimerException.h"
#import "except/SDLThreadException.h"
#import "except/SDLSemException.h"
#import "except/SDLMutexException.h"
#import "except/SDLCondException.h"
#import "context/SDLContext.h"
#import "window/SDLWindow.h"
#import "window/SDLSurface.h"
#import "util/SDLRect.h"
#import "util/SDLColor.h"
#import "util/SDLPoint.h"
#import "util/SDLCircle.h"
#import "util/SDLKeycode.h"
#import "util/SDLClipboard.h"
#import "element/SDLElement.h"
#import "element/SDLImgElement.h"
#import "element/SDLFont.h"
#import "element/SDLTextElement.h"
#import "event/SDLEventType.h"
#import "event/SDLEvent.h"
#import "event/SDLEventHandler.h"
#import "render/SDLRenderer.h"
#import "render/SDLTexture.h"
#import "render/SDLSpriteSheet.h"
#import "render/SDLAnimation.h"
#import "render/SDLFlipType.h"
#import "gui/SDLButtonCallback.h"
#import "gui/SDLButton.h"
#import "gui/SDLSimpleMsgBoxType.h"
#import "gui/SDLSimpleMsgBox.h"
#import "sound/SDLMusic.h"
#import "sound/SDLSoundEffect.h"
#import "time/SDLTimer.h"
#import "time/SDLTimerCallback.h"
#import "time/SDLCountdownTimer.h"
#import "thread/SDLThreadCallback.h"
#import "thread/SDLThread.h"
#import "thread/SDLSemaphore.h"
#import "thread/SDLSpinLock.h"
#import "thread/SDLMutex.h"
#import "thread/SDLCondition.h"

//end of header
