/*
 * SDLElement.h
 * Declares a class that represents a graphics element
 * Created by Andrew Davis
 * Created on 9/10/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//forward declare the SDLSurface class
@class SDLSurface;

//class declaration
@interface SDLElement : NSObject {
	//field
	SDLSurface* _surface; //the surface data for the element
}

//property declaration
@property (readonly) SDLSurface* surface;

//method declarations

//initializes an SDLElement instance
- (id) initWithSurface: (SDLSurface*) newSurface;

//deallocates an SDLElement instance
- (void) dealloc;

@end //end of header
