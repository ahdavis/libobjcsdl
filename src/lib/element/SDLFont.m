/*
 * SDLFont.m
 * Implements a class that represents a TrueType font
 * Created on 10/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLFont.h"

//import the SDLFontException class header
#import "../except/SDLFontException.h"

//class implementation
@implementation SDLFont

//property synthesis
@synthesize path = _path;
@synthesize size = _size;

//init method - initializes an SDLFont instance
- (id) initWithPath: (NSString*) newPath andSize: (int) newSize {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the data pointer
		_data = TTF_OpenFont([newPath UTF8String], newSize);

		//verify that the initialization succeeded
		if(_data == NULL) { //if initialization failed
			//then throw an exception
			@throw [SDLFontException 
				exceptionWithPath: newPath];
		}

		//and init the other fields
		_path = newPath;
		[_path retain];
		_size = newSize;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLFont instance
- (void) dealloc {
	TTF_CloseFont(_data); //destroy the data pointer
	_data = NULL; //and zero it out
	[_path release]; //release the path field
	[super dealloc]; //and call the superclass dealloc method
}

@end //end of implementation
