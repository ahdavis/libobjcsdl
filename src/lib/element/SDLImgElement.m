/*
 * SDLImgElement.m
 * Implements a class that represents an image element
 * Created by Andrew Davis
 * Created on 9/10/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLImgElement.h"

//import the SDLSurface header
#import "../window/SDLSurface.h"

//import the SDL library header
#import <SDL2/SDL.h>

//import the SDLOptException header
#import "../except/SDLOptException.h"

//import the SDLSurface+Data category header
#import "../window/SDLSurface+Data.h"

//class implementation
@implementation SDLImgElement

//property synthesis
@synthesize imagePath = _imagePath;

//init method - initializes an SDLImgElement instance
- (id) initWithImagePath: (NSString*) newImagePath {
	//call the superclass init method
	self = [super initWithSurface: [[SDLSurface alloc] 
		    		initWithImagePath: newImagePath]];

	//verify that the call was successful
	if(self) { //if the call was successful
		//then init the field
		_imagePath = newImagePath;
		[_imagePath retain];
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLImgElement instance
- (void) dealloc {
	[_imagePath release]; //release the image path field
	[super dealloc]; //and call the superclass dealloc method
}

//optimizeForSurface method - optimizes an SDLImgElement
//for a given surface
- (void) optimizeForSurface: (SDLSurface*) surface {
	//make sure that the element is not being optimized
	//for its own surface
	if(_surface == surface) { //if the field matches the argument
		return; //then exit the method
	}

	//declare the optimized surface
	SDL_Surface* optimizedSurface = NULL;

	//attempt to optimize the element
	optimizedSurface = SDL_ConvertSurface([_surface data],
				[surface data]->format, 0);

	//verify that the optimization was successful
	if(optimizedSurface == NULL) { //if the optimization failed
		//then throw an exception
		@throw [SDLOptException exceptionWithImagePath:
				_imagePath];
	}

	//if control reaches here, then optimization was successful
	
	//free the old surface
	SDL_FreeSurface([_surface data]);

	//and update the surface field
	[_surface setData: optimizedSurface];
}

//addColorKeyForColor method - enables color keying for
//a specific background color
- (void) addColorKeyForColor: (SDLColor*) color {
	//set the color key
	SDL_SetColorKey([_surface data], SDL_TRUE,
			SDL_MapRGBA([_surface data]->format,
					[color red],
					[color green],
					[color blue],
					[color alpha]));
}

//removeColorKeyForColor method - disables color keying for
//a specific background color
- (void) removeColorKeyForColor: (SDLColor*) color {
	//remove the color key
	SDL_SetColorKey([_surface data], SDL_FALSE,
			SDL_MapRGBA([_surface data]->format,
					[color red],
					[color green],
					[color blue],
					[color alpha]));
}

@end //end of implementation
