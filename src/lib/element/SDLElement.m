/*
 * SDLElement.m
 * Implements a class that represents a graphics element
 * Created by Andrew Davis
 * Created on 9/10/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLElement.h"

//import the SDLSurface header
#import "../window/SDLSurface.h"

//class implementation
@implementation SDLElement

//property synthesis
@synthesize surface = _surface;

//init method - initializes an SDLElement instance
- (id) initWithSurface: (SDLSurface*) newSurface {
	//call the superclass init method
	self = [super init];

	//verify that the call was successful
	if(self) { //if the call was successful
		//then init the field
		_surface = newSurface;
		[_surface retain];
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLElement instance
- (void) dealloc {
	[_surface release]; //release the surface field
	[super dealloc]; //and call the superclass dealloc method
}

@end //end of implementation
