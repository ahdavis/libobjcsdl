/*
 * SDLTextElement.m
 * Implements a class that represents renderable text
 * Created on 10/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLTextElement.h"

//import the SDLSurface class header
#import "../window/SDLSurface.h"

//class implementation
@implementation SDLTextElement

//property synthesis
@synthesize text = _text;
@synthesize font = _font;
@synthesize color = _color;

//init method - initializes an SDLTextElement instance
- (id) initWithText: (NSString*) newText andFont: (SDLFont*) newFont
	withColor: (SDLColor*) newColor {
	//call the superclass init method
	self = [super initWithSurface: [[SDLSurface alloc] 
			 		initWithText: newText
					     andFont: newFont
					   withColor: newColor]];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_text = newText;
		_font = newFont;
		_color = newColor;
		[_text retain];
		[_font retain];
		[_color retain];
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLTextElement instance
- (void) dealloc {
	[_text release]; //release the text
	[_font release]; //release the font
	[_color release]; //release the color
	[super dealloc]; //and call the superclass dealloc method
}

@end //end of implementation
