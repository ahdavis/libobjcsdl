/*
 * SDLImgElement.h
 * Declares a class that represents an image element
 * Created by Andrew Davis
 * Created on 9/10/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "SDLElement.h"
#import "../util/SDLColor.h"

//forward declare the SDLSurface class
@class SDLSurface;

//class declaration
@interface SDLImgElement : SDLElement {
	//field
	NSString* _imagePath; //the path to the source image file
}

//property declaration
@property (readonly) NSString* imagePath;

//method declarations

//initializes an SDLImgElement instance
- (id) initWithImagePath: (NSString*) newImagePath;

//deallocates an SDLImgElement instance
- (void) dealloc;

//optimizes an SDLImgElement instance for a specific surface
- (void) optimizeForSurface: (SDLSurface*) surface;

//color keys an SDLImgElement instance for a specific background color
- (void) addColorKeyForColor: (SDLColor*) color;

//removes the color key for a specific background color
- (void) removeColorKeyForColor: (SDLColor*) color;

@end //end of header
