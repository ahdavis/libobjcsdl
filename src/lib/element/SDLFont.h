/*
 * SDLFont.h
 * Declares a class that represents a TrueType font
 * Created on 10/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <SDL2/SDL_ttf.h>

//class declaration
@interface SDLFont : NSObject {
	//fields
	TTF_Font* _data; //the font data
	NSString* _path; //the path to the font's file
	int _size; //the size of the font
}

//property declarations
@property (readonly) NSString* path;
@property (readonly) int size;

//method declarations

//initializes an SDLFont instance
- (id) initWithPath: (NSString*) newPath andSize: (int) newSize;

//deallocates an SDLFont instance
- (void) dealloc;

@end //end of header
