/*
 * SDLTextElement.h
 * Declares a class that represents renderable text
 * Created on 10/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "SDLElement.h"
#import "SDLFont.h"
#import "../util/SDLColor.h"

//class declaration
@interface SDLTextElement : SDLElement {
	//fields
	NSString* _text; //the text string being represented
	SDLFont* _font; //the font of the text string
	SDLColor* _color; //the color of the text string
}

//property declarations
@property (readonly) NSString* text;
@property (readonly) SDLFont* font;
@property (readonly) SDLColor* color;

//method declarations

//initializes an SDLTextElement instance
- (id) initWithText: (NSString*) newText andFont: (SDLFont*) newFont
	withColor: (SDLColor*) newColor;

//deallocates an SDLTextElement instance
- (void) dealloc;

@end //end of header
