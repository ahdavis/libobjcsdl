/*
 * SDLFont+Data.h
 * Declares a category that allows access to an SDLFont's data pointer
 * Created on 10/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import "SDLFont.h"
#import <SDL2/SDL_ttf.h>

//category declaration
@interface SDLFont (Data)

//method declaration

//returns the SDLFont's data pointer
- (TTF_Font*) data;

@end
