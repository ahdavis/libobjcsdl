/*
 * SDLFont+Data.m
 * Implements a category that allows access to an SDLFont's data pointer
 * Created on 10/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the category header
#import "SDLFont+Data.h"

//category implementation
@implementation SDLFont (Data)

//data method - returns the SDLFont's data pointer
- (TTF_Font*) data {
	return _data; //return the data pointer
}

@end //end of implementation
