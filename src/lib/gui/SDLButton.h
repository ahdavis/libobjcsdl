/*
 * SDLButton.h
 * Declares a class that represents a button
 * Created on 10/5/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "SDLButtonCallback.h"
#import "../render/SDLTexture.h"
#import "../util/SDLRect.h"
#import "../event/SDLEvent.h"
#import "../context/SDLContext.h"

//class declaration
@interface SDLButton : NSObject {
	//fields
	SDLButtonCallback _callback; //the button's callback function
	SDLTexture* _texture; //the button's texture
	SDLRect* _bounds; //the button's bounding box
}

//property declarations
@property (readwrite) SDLButtonCallback callback;
@property (retain) SDLTexture* texture;
@property (readonly) SDLRect* bounds;

//method declarations

//initializes an SDLButton instance with an SDLRect bounding box
- (id) initWithCallback: (SDLButtonCallback) newCallback
	andTexture: (SDLTexture*) newTexture
	andBounds: (SDLRect*) newBounds;

//initializes an SDLButton instance with bounding components
- (id) initWithCallback: (SDLButtonCallback) newCallback
	andTexture: (SDLTexture*) newTexture
	andX: (int) x andY: (int) y
	andWidth: (int) width andHeight: (int) height;

//deallocates an SDLButton instance
- (void) dealloc;

//handles a click event for the SDLButton
- (void) onClickEvent: (SDLEvent*) event withContext: (SDLContext*) ctxt;

@end //end of header
