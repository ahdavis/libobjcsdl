/*
 * SDLButtonCallback.h
 * Defines a typedef for button callback functions
 * Created on 10/5/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import "../event/SDLEvent.h"
#import "../context/SDLContext.h"

//forward declare the SDLButton class
@class SDLButton;

//typedef definition
typedef void (*SDLButtonCallback)(SDLButton*, SDLEvent*, SDLContext*);

//end of header
