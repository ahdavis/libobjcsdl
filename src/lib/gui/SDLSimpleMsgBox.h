/*
 * SDLSimpleMsgBox.h
 * Declares a class that represents a simple message box
 * Created on 10/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "SDLSimpleMsgBoxType.h"
#import "../window/SDLWindow.h"

//class declaration
@interface SDLSimpleMsgBox : NSObject {
	//fields
	SDLSimpleMsgBoxType _type; //the type of the message box
	NSString* _title; //the title of the message box
	NSString* _message; //the message to be shown
	SDLWindow* _parentWindow; //the parent window for the message box
}

//property declarations
@property (readwrite) SDLSimpleMsgBoxType type;
@property (retain) NSString* title;
@property (retain) NSString* message;
@property (retain) SDLWindow* parentWindow;

//method declarations

//initializes an SDLSimpleMsgBox instance with no parent window
- (id) initWithType: (SDLSimpleMsgBoxType) newType
	andTitle: (NSString*) newTitle
	andMessage: (NSString*) newMessage;

//initializes an SDLSimpleMsgBox instance with a parent window
- (id) initWithType: (SDLSimpleMsgBoxType) newType
	andTitle: (NSString*) newTitle
	andMessage: (NSString*) newMessage
	andParentWindow: (SDLWindow*) newParentWindow;

//deallocates an SDLSimpleMsgBox instance
- (void) dealloc;

//shows the message box
- (void) show;

@end //end of header
