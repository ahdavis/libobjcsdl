/*
 * SDLSimpleMsgBoxType.h
 * Enumerates simple message box types
 * Created on 10/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//include
#include <SDL2/SDL.h>

//enum definition
typedef enum {
	SDL_MSGBOX_ERR = SDL_MESSAGEBOX_ERROR, //error dialog
	SDL_MSGBOX_WARN = SDL_MESSAGEBOX_WARNING, //warning dialog
	SDL_MSGBOX_INFO = SDL_MESSAGEBOX_INFORMATION //info dialog
} SDLSimpleMsgBoxType;

//end of header
