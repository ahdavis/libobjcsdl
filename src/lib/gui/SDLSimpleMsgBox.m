/*
 * SDLSimpleMsgBox.m
 * Implements a class that represents a simple message box
 * Created on 10/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLSimpleMsgBox.h"

//import the SDLSimpleMsgBoxException class header
#import "../except/SDLSimpleMsgBoxException.h"

//import the SDLWindow+Data category header
#import "../window/SDLWindow+Data.h"

//import the SDL library header
#import <SDL2/SDL.h>

//class implementation
@implementation SDLSimpleMsgBox

//property synthesis
@synthesize type = _type;
@synthesize title = _title;
@synthesize message = _message;
@synthesize parentWindow = _parentWindow;

//first init method - initializes an SDLSimpleMsgBox instance
//with no parent window
- (id) initWithType: (SDLSimpleMsgBoxType) newType
	andTitle: (NSString*) newTitle
      andMessage: (NSString*) newMessage {
	//call the other init method
	return [self initWithType: newType andTitle: newTitle
			andMessage: newMessage andParentWindow: nil];
}

//second init method - initializes an SDLSimpleMsgBox instance
//with a parent window
- (id) initWithType: (SDLSimpleMsgBoxType) newType
	andTitle: (NSString*) newTitle
     	 andMessage: (NSString*) newMessage
	 andParentWindow: (SDLWindow*) newParentWindow {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_type = newType;
		_title = newTitle;
		[_title retain];
		_message = newMessage;
		[_message retain];
		_parentWindow = newParentWindow;
		[_parentWindow retain];
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLSimpleMsgBox instance
- (void) dealloc {
	[_title release]; //release the title
	[_message release]; //release the message
	[_parentWindow release]; //release the parent window
	[super dealloc]; //and call the superclass dealloc method
}

//show method - shows the message box
- (void) show {
	//get the parent window data
	SDL_Window* parentData = _parentWindow == nil ? NULL : 
						[_parentWindow data];

	//attempt to show the message box
	int res = SDL_ShowSimpleMessageBox(_type,
					[_title UTF8String],
					[_message UTF8String],
					parentData);

	//and make sure that the showing succeeded
	if(res < 0) { //if the showing failed
		//then throw an exception
		@throw [SDLSimpleMsgBoxException 
			exceptionWithTitle: _title];
	}
}

@end //end of implementation
