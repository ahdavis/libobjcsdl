/*
 * SDLButton.m
 * Implements a class that represents a button
 * Created on 10/5/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLButton.h"

//class implementation
@implementation SDLButton

//property synthesis
@synthesize callback = _callback;
@synthesize texture = _texture;
@synthesize bounds = _bounds;

//first init method - initializes an SDLButton instance
//with an SDLPoint position
- (id) initWithCallback: (SDLButtonCallback) newCallback
	andTexture: (SDLTexture*) newTexture
	 andBounds: (SDLRect*) newBounds {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_callback = newCallback;
		_texture = newTexture;
		[_texture retain];
		_bounds = newBounds;
		[_bounds retain];
	}

	//and return the instance
	return self;
}

//second init method - initializes an SDLButton instance
//with bounding components
- (id) initWithCallback: (SDLButtonCallback) newCallback
	andTexture: (SDLTexture*) newTexture
	      andX: (int) x andY: (int) y 
		andWidth: (int) width andHeight: (int) height {
	//call the first init method
	return [self initWithCallback: newCallback
			   andTexture: newTexture
			  andBounds: [[SDLRect alloc] initWithX: x
			  			andY: y
						andWidth: width
						andHeight: height]];
}

//dealloc method - deallocates an SDLButton instance
- (void) dealloc {
	[_texture release]; //release the texture field
	[_bounds release]; //release the bounds field
	[super dealloc]; //and call the superclass dealloc method
}

//onClickEvent method - handles click events for the button
- (void) onClickEvent: (SDLEvent*) event 
		withContext: (SDLContext*) ctxt {
	_callback(self, event, ctxt); //execute the callback function
}

@end //end of implementation
