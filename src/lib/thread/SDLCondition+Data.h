/*
 * SDLCondition+Data.h
 * Declares a category that allows access to an SDLCondition's data pointer
 * Created by Andrew Davis
 * Created on 2/10/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import "SDLCondition.h"
#import <SDL2/SDL_thread.h>

//category declaration
@interface SDLCondition (Data)

//returns the data pointer of the condition
- (SDL_cond*) data;

@end //end of header
