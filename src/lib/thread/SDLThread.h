/*
 * SDLThread.h
 * Declares a class that represents an execution thread
 * Created by Andrew Davis
 * Created on 2/7/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <SDL2/SDL_thread.h>
#import "SDLThreadCallback.h"

//class declaration
@interface SDLThread : NSObject {
	//fields
	SDL_Thread* _data; //the internal SDL data
	NSMutableData* _threadData; //the data to pass to the callback fn
	NSString* _name; //the name of the thread
	SDLThreadCallback _callback; //the thread callback function
	bool _started; //has execution of the thread started?
}

//property declarations
@property (retain) NSString* name;
@property (readwrite) SDLThreadCallback callback;
@property (readonly) bool started;

//method declarations

//initializes an SDLThread instance, passing a number to the callback
- (id) initWithName: (NSString*) newName
	andNumber: (NSNumber*) newData 
	andCallback: (SDLThreadCallback) newCallback;

//initializes an SDLThread instance, passing a string to the callback
- (id) initWithName: (NSString*) newName
	andString: (NSString*) newData 
	andCallback: (SDLThreadCallback) newCallback;


//initializes an SDLThread instance, passing raw data to the callback
- (id) initWithName: (NSString*) newName 
	andData: (NSMutableData*) newData
	andCallback: (SDLThreadCallback) newCallback;

//deallocates an SDLThread instance
- (void) dealloc;

//starts executing the thread and throws an SDLThreadException
//if the thread cannot be initialized
- (void) run;

//blocks the calling thread until this thread finishes
//and returns the return value of the thread function
- (int) wait;

@end //end of header
