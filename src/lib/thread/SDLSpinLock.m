/*
 * SDLSpinLock.m
 * Implements a class that represents a spinlock
 * Created by Andrew Davis
 * Created on 2/10/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLSpinLock.h"

//class implementation
@implementation SDLSpinLock

//property synthesis
@synthesize locked = _locked;

//init method - initializes an SDLSpinLock instance
- (id) init {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_data = 0;
		_locked = NO;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLSpinLock instance
- (void) dealloc {
	[self unlock]; //unlock the spinlock if it is locked
	[super dealloc]; //and call the superclass dealloc method
}

//lock method - locks the SDLSpinLock
- (void) lock {
	SDL_AtomicLock(&_data); //lock the spinlock
	_locked = YES; //and set the locked flag
}

//unlock method - unlocks the SDLSpinLock
- (void) unlock {
	SDL_AtomicUnlock(&_data); //unlock the spinlock
	_locked = NO; //and clear the locked flag
}

@end //end of implementation
