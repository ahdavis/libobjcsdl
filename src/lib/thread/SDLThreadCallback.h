/*
 * SDLThreadCallback.h
 * Defines a type that specifies thread callback function signatures
 * Created by Andrew Davis
 * Created on 2/7/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//no imports

//typedef definition
typedef int (*SDLThreadCallback)(void*);

//end of file
