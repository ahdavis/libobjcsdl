/*
 * SDLSemaphore.m
 * Implements a class that represents a semaphore
 * Created by Andrew Davis
 * Created on 2/7/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLSemaphore.h"

//import the SDLSemException class header
#import "../except/SDLSemException.h"

//class implementation
@implementation SDLSemaphore

//property synthesis
@synthesize locked = _locked;

//init method - initializes an SDLSemaphore instance
- (id) initWithLockCount: (NSUInteger) lockCount {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		
		//attempt to init the data pointer
		_data = SDL_CreateSemaphore(lockCount);

		//verify that initialization succeeded
		if(_data == NULL) { //if initialization failed
			//then throw an exception
			@throw [SDLSemException 
				exceptionWithLockCount: lockCount];
		}

		//init the locked flag
		_locked = NO;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLSemaphore instance
- (void) dealloc {
	SDL_DestroySemaphore(_data); //destroy the data pointer
	_data = NULL; //and zero it out
	[super dealloc]; //and call the superclass dealloc method
}

//value method - returns the current value of the semaphore
- (NSUInteger) value {
	return SDL_SemValue(_data);
}

//wait method - waits the semaphore
- (void) wait {
	//wait the semaphore
	SDL_SemWait(_data);

	//and update the locked field
	if(self.value == 0) {
		_locked = YES;
	} else {
		_locked = NO;
	}
}

//post method - posts the semaphore
- (void) post {
	//post the semaphore
	SDL_SemPost(_data);

	//and update the locked field
	_locked = NO;
}

@end //end of implementation
