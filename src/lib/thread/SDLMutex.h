/*
 * SDLMutex.h
 * Declares a class that represents a mutex
 * Created by Andrew Davis
 * Created on 2/10/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <SDL2/SDL_thread.h>

//class declaration
@interface SDLMutex : NSObject {
	//fields
	SDL_mutex* _data; //the internal SDL data pointer
	bool _locked; //is the mutex locked?
}

//property declaration
@property (readonly) bool locked;

//method declarations

//initializes an SDLMutex instance
- (id) init;

//deallocates an SDLMutex instance
- (void) dealloc;

//locks the mutex
- (void) lock;

//unlocks the mutex
- (void) unlock;

@end //end of header
