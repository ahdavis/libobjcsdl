/*
 * SDLCondition.m
 * Implements a class that represents a thread condition
 * Created by Andrew Davis
 * Created on 2/10/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLCondition.h"

//import the SDLCondException class header
#import "../except/SDLCondException.h"

//import the SDLCondition+Data category header
#import "SDLCondition+Data.h"

//import the SDLMutex+Data category header
#import "SDLMutex+Data.h"

//class implementation
@implementation SDLCondition

//init method - initializes an SDLCondition instance
- (id) init {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the field
		_data = SDL_CreateCond();

		//and verify that initialization succeeded
		if(_data == NULL) { 
			@throw [SDLCondException exception];
		}
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLCondition instance
- (void) dealloc {
	SDL_DestroyCond(_data); //destroy the data pointer
	_data = NULL; //zero it out
	[super dealloc]; //and call the superclass dealloc method
}

//waitWithMutex method - blocks the calling thread until
//a signal is received from another Condition object
- (void) waitWithMutex: (SDLMutex*) mutex {
	SDL_CondWait(_data, mutex.data); 
}

//signal method - signals another Condition object to
//restart its thread
- (void) signal: (SDLCondition*) cond {
	SDL_CondSignal(cond.data);
}

@end //end of implementation
