/*
 * SDLThread.m
 * Implements a class that represents an execution thread
 * Created by Andrew Davis
 * Created on 2/7/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLThread.h"

//import the SDLThreadException class header
#import "../except/SDLThreadException.h"

//class implementation
@implementation SDLThread

//property synthesis
@synthesize name = _name;
@synthesize callback = _callback;
@synthesize started = _started;

//first init method - initializes an SDLThread instance,
//passing a number to the callback function
- (id) initWithName: (NSString*) newName
	andNumber: (NSNumber*) newData
      andCallback: (SDLThreadCallback) newCallback {
	//call the third init method
	return [self initWithName: newName 
		andData: [[NSKeyedArchiver 
			archivedDataWithRootObject: newData] 
				mutableCopy]
				andCallback: newCallback];
}

//second init method - initializes an SDLThread instance,
//passing a string to the callback function
- (id) initWithName: (NSString*) newName
	andString: (NSString*) newData
      andCallback: (SDLThreadCallback) newCallback {
	//get the length of the string
	NSUInteger cStrLen = [newData lengthOfBytesUsingEncoding:
				NSUTF8StringEncoding];

	//call the third init method
	return [self initWithName: newName
			  andData: [[NSMutableData alloc] 
			    initWithBytes: newData.UTF8String 
			   	length: cStrLen + 1]
		      andCallback: newCallback];
}

//third init method - initializes an SDLThread instance,
//passing raw data to the callback function
- (id) initWithName: (NSString*) newName
	andData: (NSMutableData*) newData
    andCallback: (SDLThreadCallback) newCallback {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_data = NULL;
		_threadData = newData;
		[_threadData retain];
		_name = newName;
		[_name retain];
		_callback = newCallback;
		_started = NO;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLThread instance
- (void) dealloc {
	[self wait]; //wait for the thread to finish
	[_threadData release]; //release the thread data
	[_name release]; //release the thread name
	[super dealloc]; //and call the superclass dealloc method
}

//run method - starts the execution of the SDLThread
- (void) run {
	//attempt to start executing the thread
	_data = SDL_CreateThread(_callback, [_name UTF8String],
					[_threadData mutableBytes]);

	//make sure that the attempt was successful
	if(_data == NULL) { //if the attempt failed
		//then throw an exception
		@throw [SDLThreadException 
				exceptionWithThreadName: _name];
	}

	//and set the started flag
	_started = YES;
}

//wait method - waits for the thread to finish and then returns
//its callback function's return value
- (int) wait {
	//declare the return value
	int returnVal = -1;

	//only wait if the thread has been initialized
	if(_data != NULL) { //if the thread has been initialized
		//then wait the thread
		SDL_WaitThread(_data, &returnVal);
		_data = NULL;
		_started = NO;
	}

	//and return the return value
	return returnVal;
}

@end //end of implementation
