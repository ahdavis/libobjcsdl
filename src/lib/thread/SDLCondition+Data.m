/*
 * SDLCondition+Data.m
 * Implements a category that allows access to an SDLCondition's data field
 * Created by Andrew Davis
 * Created on 2/10/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the category header
#import "SDLCondition+Data.h"

//category implementation
@implementation SDLCondition (Data)

//data method - returns the SDLCondition's data pointer
- (SDL_cond*) data {
	return _data; //return the data pointer
}

@end //end of implementation
