/*
 * SDLMutex+Data.m
 * Implements a category that allows access to an SDLMutex's data pointer
 * Created by Andrew Davis
 * Created on 2/10/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the category header
#import "SDLMutex+Data.h"

//category implementation
@implementation SDLMutex (Data)

//data method - returns the SDLMutex's data pointer
- (SDL_mutex*) data {
	return _data; //return the data pointer
}

@end //end of implementation
