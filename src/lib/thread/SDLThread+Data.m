/*
 * SDLThread+Data.m
 * Implements a category that allows access to an SDLThread's data pointer
 * Created by Andrew Davis
 * Created on 2/7/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the category header
#import "SDLThread+Data.h"

//category implementation
@implementation SDLThread (Data)

//data method - returns the SDLThread's data pointer
- (SDL_Thread*) data {
	return _data; //return the data field
}

@end //end of implementation
