/*
 * SDLSpinLock.h
 * Declares a class that represents a spinlock
 * Created by Andrew Davis
 * Created on 2/10/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <SDL2/SDL_thread.h>

//class declaration
@interface SDLSpinLock : NSObject {
	//fields
	SDL_SpinLock _data; //the internal SDL data
	bool _locked; //is the spinlock locked?
}

//property declaration
@property (readonly) bool locked;

//method declarations

//initializes an SDLSpinLock instance
- (id) init;

//deallocates an SDLSpinLock instance
- (void) dealloc;

//locks the SDLSpinLock
- (void) lock;

//unlocks the SDLSpinLock
- (void) unlock;

@end //end of header
