/*
 * SDLMutex.m
 * Implements a class that represents a mutex
 * Created by Andrew Davis
 * Created on 2/10/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLMutex.h"

//import the SDLMutexException class header
#import "../except/SDLMutexException.h"

//class implementation
@implementation SDLMutex

//property synthesis
@synthesize locked = _locked;

//init method - initializes an SDLMutex instance
- (id) init {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		
		//attempt to initialize the data pointer
		_data = SDL_CreateMutex();
		if(_data == NULL) { //if initialization failed
			//then throw an exception
			@throw [SDLMutexException exception];
		}

		//and init the locked flag
		_locked = NO;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLMutex instance
- (void) dealloc {
	[self unlock]; //unlock the mutex if it's locked
	SDL_DestroyMutex(_data); //destroy the data pointer
	_data = NULL; //zero it out
	[super dealloc]; //and call the superclass dealloc method
}

//lock method - locks the mutex
- (void) lock {
	SDL_LockMutex(_data); //lock the mutex
	_locked = YES; //and set the locked flag
}

//unlock method - unlocks the mutex
- (void) unlock {
	SDL_UnlockMutex(_data); //unlock the mutex
	_locked = NO; //and clear the locked flag
}

@end //end of implementation
