/*
 * SDLThread+Data.h
 * Declares a category that allows access to an SDLThread's data pointer
 * Created by Andrew Davis
 * Created on 2/7/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#include <SDL2/SDL_thread.h>
#import "SDLThread.h"

//category declaration
@interface SDLThread (Data)

//method declaration

//returns the thread's data pointer
- (SDL_Thread*) data;

@end //end of header
