/*
 * SDLSemaphore.h
 * Declares a class that represents a semaphore
 * Created by Andrew Davis
 * Created on 2/7/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <SDL2/SDL.h>

//class declaration
@interface SDLSemaphore : NSObject {
	//fields
	SDL_sem* _data; //the internal SDL data pointer
	bool _locked; //is the semaphore locked?
}

//property declaration
@property (readonly) bool locked;

//method declarations

//initializes an SDLSemaphore instance
- (id) initWithLockCount: (NSUInteger) lockCount;

//deallocates an SDLSemaphore instance
- (void) dealloc;

//returns the current value of the semaphore
- (NSUInteger) value;

//waits the semaphore and locks it if its value reaches zero
- (void) wait;

//posts the semaphore and alerts waiting threads to retry a wait
- (void) post;

@end //end of header
