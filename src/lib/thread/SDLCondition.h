/*
 * SDLCondition.h
 * Declares a class that represents a thread condition
 * Created by Andrew Davis
 * Created on 2/10/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <SDL2/SDL_thread.h>
#import "SDLMutex.h"

//class declaration
@interface SDLCondition : NSObject {
	//field
	SDL_cond* _data; //the internal SDL data pointer
}

//no properties

//method declarations

//initializes an SDLCondition instance
- (id) init;

//deallocates an SDLCondition instance
- (void) dealloc;

//blocks the calling thread until a signal is received from
//another Condition object
- (void) waitWithMutex: (SDLMutex*) mutex;

//signals another condition to restart its thread
- (void) signal: (SDLCondition*) cond;

@end //end of header
