/*
 * SDLContext.m
 * Implements a class that holds contextual data for libobjcsdl
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLContext.h"

//import the SDLInitException header
#import "../except/SDLInitException.h"

//import the SDL library header
#import <SDL2/SDL.h>

//import the SDL_Image library header
#import <SDL2/SDL_image.h>

//import the SDL_TTF library header
#import <SDL2/SDL_ttf.h>

//import the SDL_Mixer library header
#import <SDL2/SDL_mixer.h>

//class implementation
@implementation SDLContext

//init method - initializes an SDLContext instance
- (id) init {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then initialize SDL
		int res = SDL_Init(SDL_INIT_VIDEO 
					| SDL_INIT_AUDIO
					| SDL_INIT_TIMER);

		//verify that the initialization succeeded
		if(res < 0) { //if the initialization failed
			//then throw an exception
			@throw [SDLInitException 
				exceptionWithLibName: @"SDL"
					 andErrorMsg: SDL_GetError()];
		}

		//initialize SDL_Image
		int imgFlags = IMG_INIT_PNG;
		if(!(IMG_Init(imgFlags) & imgFlags)) {
			//initialization failed, so throw an exception
			@throw [SDLInitException 
				exceptionWithLibName: @"SDL_image"
					andErrorMsg: IMG_GetError()];
		}

		//initialize SDL_TTF
		if(TTF_Init() == -1) {
			//initialization failed, so throw an exception
			@throw [SDLInitException 
				exceptionWithLibName: @"SDL_ttf"
					andErrorMsg: TTF_GetError()];
		}

		//initialize SDL_Mixer
		if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
			//initialization failed, so throw an exception
			@throw [SDLInitException
				exceptionWithLibName: @"SDL_mixer"
					andErrorMsg: Mix_GetError()];
		}
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLContext instance
- (void) dealloc {
	Mix_Quit(); //shut down SDL_mixer
	TTF_Quit(); //shut down SDL_ttf
	IMG_Quit(); //shut down SDL_image
	SDL_Quit(); //shut down SDL
	[super dealloc]; //and call the superclass dealloc method
}

//delayMilliseconds method - delays for a specified number of milliseconds
- (void) delayMilliseconds: (int) milliseconds {
	SDL_Delay(milliseconds); //delay for the specified length
}

//delaySeconds method - delays for a specified number of seconds
- (void) delaySeconds: (int) seconds {
	//call the other delay method
	[self delayMilliseconds: seconds * 1000]; 
}

//mouseCoords method - returns an autoreleased SDLPoint instance
//containing the current coordinates of the mouse
- (SDLPoint*) mouseCoords {
	int x, y; //will hold the coordinates
	SDL_GetMouseState(&x, &y); //get the coords of the mouse

	//and return an SDLPoint instance containing the coords
	return [[[SDLPoint alloc] initWithX: x andY: y] autorelease];
}

//ticksSinceStart method - returns the number of milliseconds
//elapsed since the program started
- (NSUInteger) ticksSinceStart {
	return SDL_GetTicks(); //return the elapsed milliseconds
}

@end //end of implementation
