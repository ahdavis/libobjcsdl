/*
 * SDLContext.h
 * Declares a class that holds contextual data for libobjcsdl
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "../util/SDLPoint.h"

//class declaration
@interface SDLContext : NSObject {
	//no fields
}

//no properties

//method declarations

//initializes an SDLContext instance
//throws an SDLInitException if initialization fails
- (id) init;

//deallocates an SDLContext instance
- (void) dealloc;

//causes a delay of a specified number of milliseconds
- (void) delayMilliseconds: (int) milliseconds;

//causes a delay of a specified number of seconds
- (void) delaySeconds: (int) seconds;

//returns an autoreleased SDLPoint instance containing
//the current coordinates of the mouse
- (SDLPoint*) mouseCoords;

//returns the number of milliseconds elapsed since the program started
- (NSUInteger) ticksSinceStart;

@end //end of header
