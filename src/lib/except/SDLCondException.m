/*
 * SDLCondException.m
 * Implements an exception that is thrown when a condition
 * fails to initialize
 * Created by Andrew Davis
 * Created on 2/10/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLCondException.h"

//import the SDL library header
#import <SDL2/SDL.h>

//class implementation
@implementation SDLCondException

//init method - initializes an SDLCondException instance
- (id) init {
	//call the superclass init method
	self = [super initWithName: @"SDLCondException"
			reason: [NSString stringWithFormat: 
			@"Failed to initialize a condition! Message: %s",
				SDL_GetError()]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLCondException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - returns an autoreleased 
//SDLCondException instance
+ (SDLCondException*) exception {
	return [[[SDLCondException alloc] init] autorelease];
}

@end //end of implementation 
