/*
 * SDLThreadException.m
 * Implements an exception that is thrown when a thread fails to initialize
 * Created by Andrew Davis
 * Created on 2/7/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLThreadException.h"

//import the SDL header
#include <SDL2/SDL.h>

//class implementation
@implementation SDLThreadException

//init method - initializes an SDLThreadException instance
- (id) initWithThreadName: (NSString*) badName {
	//call the superclass init method
	self = [super initWithName: @"SDLThreadException"
		reason: [NSString stringWithFormat:
		@"Failed to create thread with name %@! Message: %s",
			badName, SDL_GetError()]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLThreadException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - returns an autoreleased
//SDLThreadException instance
+ (SDLThreadException*) exceptionWithThreadName: (NSString*) badName {
	return [[[SDLThreadException alloc] initWithThreadName: badName]
		autorelease];
}

@end //end of implementation
