/*
 * SDLTextureException.h
 * Declares an exception that is thrown when a texture fails to initialize
 * Created by Andrew Davis
 * Created on 9/14/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLTextureException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLTextureException instance from an image path
- (id) initWithImagePath: (NSString*) imagePath;

//initializes an SDLTextureException instance from a text string
- (id) initWithText: (NSString*) text;

//deallocates an SDLTextureException instance
- (void) dealloc;

//generates an autoreleased SDLTextureException instance from an image path
+ (SDLTextureException*) exceptionWithImagePath: (NSString*) imagePath;

//generates an autoreleased SDLTextureException instance from a text string
+ (SDLTextureException*) exceptionWithText: (NSString*) text;

@end //end of header
