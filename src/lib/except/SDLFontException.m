/*
 * SDLFontException.m
 * Implements an exception that is thrown when a font fails to load
 * Created on 10/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLFontException.h"

//import the SDL_ttf library header
#import <SDL2/SDL_ttf.h>

//class implementation
@implementation SDLFontException

//init method - initializes an SDLFontException instance
- (id) initWithPath: (NSString*) path {
	//call the superclass init method
	self = [super initWithName: @"SDLFontException"
	reason:
[NSString stringWithFormat: @"Failed to load font %@! Message: %s",
	path, TTF_GetError()]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLFontException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased 
//SDLFontException instance
+ (SDLFontException*) exceptionWithPath: (NSString*) path {
	//return an autoreleased instance
	return [[[SDLFontException alloc] initWithPath: path] autorelease];
}

@end //end of implementation
