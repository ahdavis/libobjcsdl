/*
 * SDLExtException.m
 * Implements an exception that is thrown when
 * a resource file path has a bad extension
 * Created by Andrew Davis
 * Created on 9/9/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLExtException.h"

//include the SDL header
#include <SDL2/SDL.h>

//class implementation
@implementation SDLExtException

//init method - initializes an SDLExtException instance
- (id) initWithPath: (NSString*) path {
	//call the superclass init method
	self = [super initWithName: @"SDLExtException"
reason: [NSString 
stringWithFormat: @"Bad extension for file at path %@.",
		path]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLExtException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - returns an autoreleased 
//SDLExtException instance
+ (SDLExtException*) exceptionWithPath: (NSString*) path {
	//return an autoreleased SDLExtException instance
	return [[[SDLExtException alloc] initWithPath: path]
			autorelease];
}

@end //end of implementation
