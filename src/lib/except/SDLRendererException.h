/*
 * SDLRendererException.h
 * Declares an exception that is thrown when a renderer fails to initialize
 * Created by Andrew Davis
 * Created on 9/14/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLRendererException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLRendererException instance
- (id) init;

//deallocates an SDLRendererException instance
- (void) dealloc;

//generates an autoreleased SDLRendererException instance
+ (SDLRendererException*) exception;

@end //end of header
