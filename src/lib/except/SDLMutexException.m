/*
 * SDLMutexException.m
 * Implements an exception that is thrown when a mutex fails to initialize
 * Created by Andrew Davis
 * Created on 2/10/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLMutexException.h"

//import the SDL library header
#import <SDL2/SDL.h>

//class implementation
@implementation SDLMutexException

//init method - initializes an SDLMutexException instance
- (id) init {
	//call the superclass init method
	self = [super initWithName: @"SDLMutexException"
			reason: [NSString stringWithFormat: 
			@"Failed to initialize a mutex! Message: %s",
				SDL_GetError()]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLMutexException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - returns an autoreleased 
//SDLMutexException instance
+ (SDLMutexException*) exception {
	return [[[SDLMutexException alloc] init] autorelease];
}

@end //end of implementation 
