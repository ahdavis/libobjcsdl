/*
 * SDLTimerException.m
 * Implements an exception that is thrown 
 * when a countdown timer fails to start
 * Created by Andrew Davis
 * Created on 12/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLTimerException.h"

//include the SDL library header
#include <SDL2/SDL.h>

//class implementation
@implementation SDLTimerException

//init method - initializes an SDLTimerException instance
- (id) init {
	//call the superclass init method
	self = [super initWithName: @"SDLTimerException"
			    reason:
	[NSString 
	stringWithFormat: @"Countdown timer failed to start! Message: %s",
		SDL_GetError()]
			userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLTimerException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased
//SDLTimerException instance
+ (SDLTimerException*) exception {
	return [[[SDLTimerException alloc] init] autorelease];
}

@end //end of implementation
