/*
 * SDLOptException.h
 * Declares an exception that is thrown when image optimization fails
 * Created by Andrew Davis
 * Created on 9/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLOptException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLOptException instance
- (id) initWithImagePath: (NSString*) imagePath;

//deallocates an SDLOptException instance
- (void) dealloc;

//generates an autoreleased SDLOptException instance
+ (SDLOptException*) exceptionWithImagePath: (NSString*) imagePath;

@end //end of header
