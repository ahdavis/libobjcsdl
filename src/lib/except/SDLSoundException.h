/*
 * SDLSoundException.h
 * Declares an exception that is thrown when sound fails to load
 * Created on 10/9/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLSoundException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLSoundException instance
- (id) initWithPath: (NSString*) badPath;

//deallocates an SDLSoundException instance
- (void) dealloc;

//generates an autoreleased SDLSoundException instance
+ (SDLSoundException*) exceptionWithPath: (NSString*) badPath;

@end //end of header
