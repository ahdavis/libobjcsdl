/*
 * SDLSemException.m
 * Implements an exception that is thrown when a semaphore
 * fails to initialize
 * Created by Andrew Davis
 * Created on 2/7/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLSemException.h"

//import the SDL library header
#import <SDL2/SDL.h>

//class implementation
@implementation SDLSemException

//init method - initializes an SDLSemException instance
- (id) initWithLockCount: (NSUInteger) badLockCount {
	//call the superclass init method
	self = [super initWithName: @"SDLSemException"
		reason: [NSString stringWithFormat: 
@"Failed to initialize a semaphore with lock count %lu! Message: %s",
			badLockCount, SDL_GetError()]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLSemException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased
//SDLSemException instance
+ (SDLSemException*) exceptionWithLockCount: (NSUInteger) badLockCount {
	return [[[SDLSemException alloc] initWithLockCount: badLockCount] 
		autorelease];
}

@end //end of implementation
