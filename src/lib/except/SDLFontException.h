/*
 * SDLFontException.h
 * Declares an exception that is thrown when a font fails to load
 * Created on 10/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLFontException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLFontException instance
- (id) initWithPath: (NSString*) path;

//deallocates an SDLFontException instance
- (void) dealloc;

//generates an autoreleased SDLFontException instance
+ (SDLFontException*) exceptionWithPath: (NSString*) path;

@end //end of header
