/*
 * SDLMutexException.h
 * Declares an exception that is thrown when a mutex fails to initialize
 * Created by Andrew Davis
 * Created on 2/10/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLMutexException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLMutexException instance
- (id) init;

//deallocates an SDLMutexException instance
- (void) dealloc;

//generates an autoreleased SDLMutexException instance
+ (SDLMutexException*) exception;

@end //end of header
