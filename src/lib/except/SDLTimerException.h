/*
 * SDLTimerException.h
 * Declares an exception that is thrown 
 * when a countdown timer fails to start
 * Created by Andrew Davis
 * Created on 12/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLTimerException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLTimerException instance
- (id) init;

//deallocates an SDLTimerException instance
- (void) dealloc;

//generates an autoreleased SDLTimerException instance
+ (SDLTimerException*) exception;

@end //end of header
