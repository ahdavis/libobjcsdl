/*
 * SDLWindowException.m
 * Implements an exception that is thrown when a window fails to initialize
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLWindowException.h"

//import the SDL header
#import <SDL2/SDL.h>

//class declaration
@implementation SDLWindowException

//init method - initializes an SDLWindowException instance
- (id) initWithTitle: (NSString*) badTitle {
	//call the superclass init method
	self = [super initWithName: @"SDLWindowException"
reason: [NSString 
stringWithFormat: @"Failed to create window with title %@! Message: %s",
		badTitle, SDL_GetError()]
			userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLWindowException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased
//SDLWindowException instance
+ (SDLWindowException*) exceptionWithTitle: (NSString*) badTitle {
	//return an autoreleased SDLWindowException instance
	return [[[SDLWindowException alloc] initWithTitle: badTitle]
		autorelease];
}

@end //end of implementation
