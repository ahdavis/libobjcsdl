/*
 * SDLOptException.m
 * Implements an exception that is thrown when image optimization fails
 * Created by Andrew Davis
 * Created on 9/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLOptException.h"

//import the SDL library header
#import <SDL2/SDL.h>

//class implementation
@implementation SDLOptException

//init method - initializes an SDLOptException instance
- (id) initWithImagePath: (NSString*) imagePath {
	//call the superclass init method
	self = [super initWithName: @"SDLOptException"
reason: [NSString 
	stringWithFormat: @"Unable to optimize image %@! Message: %s",
		imagePath, SDL_GetError()]
			userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLOptException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - returns an autoreleased SDLOptException instance
+ (SDLOptException*) exceptionWithImagePath: (NSString*) imagePath {
	//return an autoreleased instance
	return [[[SDLOptException alloc] initWithImagePath: imagePath]
			autorelease];
}

@end //end of implementation
