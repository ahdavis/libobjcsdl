/*
 * SDLWindowException.h
 * Declares an exception that is thrown when a window fails to initialize
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLWindowException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLWindowException instance
- (id) initWithTitle: (NSString*) badTitle;

//deallocates an SDLWindowException instance
- (void) dealloc;

//generates an autoreleased SDLWindowException instance
+ (SDLWindowException*) exceptionWithTitle: (NSString*) badTitle;

@end //end of header
