/*
 * SDLInitException.m
 * Implements an exception that is thrown when SDL fails to initialize
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLInitException.h"

//class implementation
@implementation SDLInitException

//init method - initializes an SDLInitException instance
- (id) initWithLibName: (NSString*) name 
	   andErrorMsg: (const char*) error {
	//call the superclass init method
	self = [super initWithName: @"SDLInitException"
reason: [NSString 
	stringWithFormat: @"Failed to initialize %@! Message: %s",
		name, error]
			userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLInitException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased SDLInitException
//instance
+ (SDLInitException*) exceptionWithLibName: (NSString*) name
	andErrorMsg: (const char*) error {
	//return an autoreleased SDLInitException instance
	return [[[SDLInitException alloc] initWithLibName: name
			andErrorMsg: error] autorelease];
}

@end //end of implementation
