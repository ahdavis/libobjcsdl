/*
 * SDLSemException.h
 * Declares an exception that is thrown when a semaphore
 * fails to initialize
 * Created by Andrew Davis
 * Created on 2/7/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLSemException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLSemException instance
- (id) initWithLockCount: (NSUInteger) badLockCount;

//deallocates an SDLSemException instance
- (void) dealloc;

//generates an autoreleased SDLSemException instance
+ (SDLSemException*) exceptionWithLockCount: (NSUInteger) badLockCount;

@end //end of header
