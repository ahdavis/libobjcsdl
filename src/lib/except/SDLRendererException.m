/*
 * SDLRendererException.m
 * Implements an exception that is thrown when a 
 * renderer fails to initialize
 * Created by Andrew Davis
 * Created on 9/14/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLRendererException.h"

//import the SDL library header
#import <SDL2/SDL.h>

//class implementation
@implementation SDLRendererException

//init method - initializes an SDLRendererException instance
- (id) init {
	//call the superclass init method
	self = [super initWithName: @"SDLRendererException"
reason: [NSString 
stringWithFormat: @"Failed to initialize renderer! Message: %s",
	SDL_GetError()]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLRendererException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased 
//SDLRendererException instance
+ (SDLRendererException*) exception {
	//return an autoreleased instance
	return [[[SDLRendererException alloc] init] autorelease];
}

@end //end of implementation
