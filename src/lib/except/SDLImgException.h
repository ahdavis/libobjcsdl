/*
 * SDLImgException.h
 * Declares an exception that is thrown when an image fails to load
 * Created by Andrew Davis
 * Created on 9/9/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLImgException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLImgException instance
- (id) initWithPath: (NSString*) path isBMP: (bool) bmpFlag;

//deallocates an SDLImgException instance
- (void) dealloc;

//generates an autoreleased SDLImgException instance
+ (SDLImgException*) exceptionWithPath: (NSString*) path
	isBMP: (bool) bmpFlag;

@end //end of header
