/*
 * SDLScaleException.m
 * Implements an exception that is thrown when 
 * a graphics element fails to scale to a target
 * Created by Andrew Davis
 * Created on 9/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLScaleException.h"

//import the SDL library header
#import <SDL2/SDL.h>

//class implementation
@implementation SDLScaleException

//init method - initializes an SDLScaleException instance
- (id) init {
	//call the superclass init method
	self = [super initWithName: @"SDLScaleException"
reason: [NSString 
stringWithFormat: @"Failed to scale element to target! Message: %s",
		SDL_GetError()]
			userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLScaleException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - returns an autoreleased SDLScaleException
//instance
+ (SDLScaleException*) exception {
	//return an autoreleased instance
	return [[[SDLScaleException alloc] init] autorelease];
}

@end //end of implementation
