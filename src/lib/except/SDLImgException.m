/*
 * SDLImgException.m
 * Implements an exception that is thrown when an image fails to load
 * Created by Andrew Davis
 * Created on 9/9/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLImgException.h"

//import the SDL header
#import <SDL2/SDL.h>

//import the SDL_image header
#import <SDL2/SDL_image.h>

//class implementation
@implementation SDLImgException

//init method - initializes an SDLImgException instance
- (id) initWithPath: (NSString*) path isBMP: (bool) bmpFlag {
	//get a function pointer to hold the error message function
	const char* (*errFunction)(void);
	
	//determine the proper error message function
	if(bmpFlag) {
		errFunction = SDL_GetError;
	} else {
		errFunction = IMG_GetError;
	}

	//call the superclass init method
	self = [super initWithName: @"SDLImgException"
reason: [NSString
stringWithFormat: @"Failed to load image at path %@. Message: %s",
		path, errFunction()]
			userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLImgException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased
//SDLImgException instance
+ (SDLImgException*) exceptionWithPath: (NSString*) path
	isBMP: (bool) bmpFlag {
	//return an autoreleased exception instance
	return [[[SDLImgException alloc] initWithPath: path
			isBMP: bmpFlag] autorelease];
}

@end //end of implementation
