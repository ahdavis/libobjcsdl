/*
 * SDLTextException.m
 * Implements an exception that is thrown when text fails to load
 * Created on 10/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLTextException.h"

//import the SDL_ttf library header
#import <SDL2/SDL_ttf.h>

//class implementation
@implementation SDLTextException

//init method - initializes an SDLTextException instance
- (id) initWithText: (NSString*) text {
	//call the superclass init method
	self = [super initWithName: @"SDLTextException"
			    reason:
[NSString stringWithFormat: @"Failed to render text %@! Message: %s",
	text, TTF_GetError()]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLTextException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased
//SDLTextException instance
+ (SDLTextException*) exceptionWithText: (NSString*) text {
	//return an autoreleased instance
	return [[[SDLTextException alloc] initWithText: text] autorelease];
}

@end //end of implementation
