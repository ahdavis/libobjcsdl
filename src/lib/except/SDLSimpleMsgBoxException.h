/*
 * SDLSimpleMsgBoxException.h
 * Declares an exception that is thrown when 
 * a simple message box fails to show
 * Created on 10/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLSimpleMsgBoxException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLSimpleMsgBoxException instance
- (id) initWithTitle: (NSString*) badTitle;

//deallocates an SDLSimpleMsgBoxException instance
- (void) dealloc;

//generates an autoreleased SDLSimpleMsgBoxException instance
+ (SDLSimpleMsgBoxException*) exceptionWithTitle: (NSString*) badTitle;

@end //end of header
