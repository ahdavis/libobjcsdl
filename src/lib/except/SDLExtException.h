/*
 * SDLExtException.h
 * Declares an exception that is thrown when
 * a resource file path has a bad extension
 * Created by Andrew Davis
 * Created on 9/9/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLExtException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLExtException instance
- (id) initWithPath: (NSString*) path;

//deallocates an SDLExtException instance
- (void) dealloc;

//returns an autoreleased SDLExtException instance
+ (SDLExtException*) exceptionWithPath: (NSString*) path;

@end //end of header
