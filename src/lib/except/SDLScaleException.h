/*
 * SDLScaleException.h
 * Declares an exception that is thrown when 
 * a graphics element fails to scale to a target
 * Created by Andrew Davis
 * Created on 9/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLScaleException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLScaleException instance
- (id) init;

//deallocates an SDLScaleException instance
- (void) dealloc;

//generates an autoreleased SDLScaleException instance
+ (SDLScaleException*) exception;

@end //end of header
