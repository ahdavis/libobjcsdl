/*
 * SDLSoundException.m
 * Implements an exception that is thrown when sound fails to load
 * Created on 10/9/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLSoundException.h"

//import the SDL_mixer library header
#import <SDL2/SDL_mixer.h>

//class implementation
@implementation SDLSoundException

//init method - initializes an SDLSoundException instance
- (id) initWithPath: (NSString*) badPath {
	//call the superclass init method
	self = [super initWithName: @"SDLSoundException"
		reason: [NSString stringWithFormat:
		@"Failed to load sound from path %@! Message: %s",
			badPath, Mix_GetError()]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLSoundException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased
//SDLSoundException instance
+ (SDLSoundException*) exceptionWithPath: (NSString*) badPath {
	//generate an autoreleased instance
	return [[[SDLSoundException alloc] initWithPath: badPath]
			autorelease];
}

@end //end of implementation
