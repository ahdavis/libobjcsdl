/*
 * SDLThreadException.h
 * Declares an exception that is thrown when a thread fails to initialize
 * Created by Andrew Davis
 * Created on 2/7/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLThreadException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLThreadException instance
- (id) initWithThreadName: (NSString*) badName;

//deallocates an SDLThreadException instance
- (void) dealloc;

//returns an autoreleased SDLThreadException instance
+ (SDLThreadException*) exceptionWithThreadName: (NSString*) badName;

@end //end of header
