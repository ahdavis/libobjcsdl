/*
 * SDLTextureException.m
 * Implements an exception that is thrown when a 
 * texture fails to initialize
 * Created by Andrew Davis
 * Created on 9/14/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLTextureException.h"

//import the SDL library header
#import <SDL2/SDL.h>

//class implementation
@implementation SDLTextureException

//first init method - initializes an SDLTextureException instance
//from an image path
- (id) initWithImagePath: (NSString*) imagePath {
	//call the superclass init method
	self = [super initWithName: @"SDLTextureException"
reason: [NSString stringWithFormat: 
	@"Unable to create texture from image with path %@! Message: %s",
	imagePath, SDL_GetError()]
		userInfo: nil];

	//and return the instance
	return self;
}

//second init method - initializes an SDLTextureException instance
//from a text string
- (id) initWithText: (NSString*) text {
	//call the superclass init method
	self = [super initWithName: @"SDLTextureException"
reason: [NSString stringWithFormat:
	@"Unable to create texture from text %@! Message: %s",
	text, SDL_GetError()]
		userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLTextureException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//first exception class method - generates an autoreleased
//SDLTextureException instance from an image path
+ (SDLTextureException*) exceptionWithImagePath: (NSString*) imagePath {
	//return an autoreleased instance
	return [[[SDLTextureException alloc] 
			initWithImagePath: imagePath]
			autorelease];
}

//second exception class method - generates an autoreleased
//SDLTextureException instance from a text string
+ (SDLTextureException*) exceptionWithText: (NSString*) text {
	//return an autoreleased instance
	return [[[SDLTextureException alloc]
			initWithText: text] autorelease];
}

@end //end of implementation
