/*
 * SDLBlitException.m
 * Implements an exception that is thrown when 
 * a graphics element fails to blit onto a target
 * Created by Andrew Davis
 * Created on 9/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLBlitException.h"

//import the SDL library header
#import <SDL2/SDL.h>

//class implementation
@implementation SDLBlitException

//init method - initializes an SDLBlitException instance
- (id) init {
	//call the superclass init method
	self = [super initWithName: @"SDLBlitException"
reason: [NSString 
stringWithFormat: @"Failed to blit element onto target! Message: %s",
		SDL_GetError()]
			userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLBlitException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased
//SDLBlitException instance
+ (SDLBlitException*) exception {
	//return an autoreleased instance
	return [[[SDLBlitException alloc] init] autorelease];
}

@end //end of implementation
