/*
 * SDLCondException.h
 * Declares an exception that is thrown when a condition 
 * fails to initialize
 * Created by Andrew Davis
 * Created on 2/10/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLCondException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLCondException instance
- (id) init;

//deallocates an SDLCondException instance
- (void) dealloc;

//generates an autoreleased SDLCondException instance
+ (SDLCondException*) exception;

@end //end of header
