/*
 * SDLInitException.h
 * Declares an exception that is thrown when SDL fails to initialize
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLInitException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLInitException instance
- (id) initWithLibName: (NSString*) name andErrorMsg: (const char*) error;

//deallocates an SDLInitException instance
- (void) dealloc;

//generates an autoreleased SDLInitException instance
+ (SDLInitException*) exceptionWithLibName: (NSString*) name
	andErrorMsg: (const char*) error;

@end //end of header
