/*
 * SDLBlitException.h
 * Declares an exception that is thrown when 
 * a graphics element fails to blit onto a target
 * Created by Andrew Davis
 * Created on 9/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLBlitException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLBlitException instance
- (id) init;

//deallocates an SDLBlitException instance
- (void) dealloc;

//generates an autoreleased SDLBlitException instance
+ (SDLBlitException*) exception;

@end //end of header
