/*
 * SDLTextException.h
 * Declares an exception that is thrown when text fails to load
 * Created on 10/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLTextException : NSException {
	//no fields
}

//no properties

//method declarations

//initializes an SDLTextException instance
- (id) initWithText: (NSString*) text;

//deallocates an SDLTextException instance
- (void) dealloc;

//generates an autoreleased SDLTextException instance
+ (SDLTextException*) exceptionWithText: (NSString*) text;

@end //end of header
