/*
 * SDLSimpleMsgBoxException.m
 * Implements an exception that is thrown when 
 * a simple message box fails to show
 * Created on 10/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLSimpleMsgBoxException.h"

//import the SDL library header
#import <SDL2/SDL.h>

//class implementation
@implementation SDLSimpleMsgBoxException

//init method - initializes an SDLSimpleMsgBoxException instance
- (id) initWithTitle: (NSString*) badTitle {
	//call the superclass init method
	self = [super initWithName: @"SDLSimpleMsgBoxException"
		reason:
[NSString stringWithFormat: 
	@"Failed to show message box with title %@! Message: %s",
		badTitle, SDL_GetError()]
			userInfo: nil];

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLSimpleMsgBoxException instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//exception class method - generates an autoreleased
//SDLSimpleMsgBoxException instance
+ (SDLSimpleMsgBoxException*) exceptionWithTitle: (NSString*) badTitle {
	//return an autoreleased instance
	return [[[SDLSimpleMsgBoxException alloc] initWithTitle: badTitle]
			autorelease];
}

@end //end of implementation
