/*
 * SDLEventHandler.h
 * Declares a class that handles input events
 * Created by Andrew Davis
 * Created on 9/10/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "SDLEvent.h"
#import "SDLEventType.h"

//class declaration
@interface SDLEventHandler : NSObject {
	//field
	SDLEvent* _currentEvent; //the most recent event polled
}

//property declaration
@property (readonly) SDLEvent* currentEvent;

//method declarations

//initializes an SDLEventHandler instance
- (id) init;

//deallocates an SDLEventHandler instance
- (void) dealloc;

//polls the next event from the queue and returns 0 if no events remain
- (int) pollNextEvent;

//blocks the calling thread until an event is triggered of a given type
- (void) waitForEventOfType: (SDLEventType) type;

@end //end of header
