/*
 * SDLEventHandler.m
 * Implements a class that handles input events
 * Created by Andrew Davis
 * Created on 9/10/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLEventHandler.h"

//import the SDL header
#import <SDL2/SDL.h>

//import the SDLEvent+Data category header
#import "SDLEvent+Data.h"

//class implementation
@implementation SDLEventHandler

//property synthesis
@synthesize currentEvent = _currentEvent;

//init method - initializes an SDLEventHandler instance
- (id) init {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the field
		_currentEvent = [[SDLEvent alloc] init];
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLEventHandler instance
- (void) dealloc {
	[_currentEvent release]; //release the current event field
	[super dealloc]; //and call the superclass dealloc method
}

//pollNextEvent method - retrieves the next event from the queue
- (int) pollNextEvent {
	//poll the next event and return the result
	return SDL_PollEvent([_currentEvent data]);
}

//waitForEventOfType method - blocks the calling thread until
//an event is read of a given type
- (void) waitForEventOfType: (SDLEventType) type {
	//loop until the polled event has a given type
	while(true) {
		//wait for the event
		SDL_WaitEvent([_currentEvent data]);

		//check to see if it was the
		//given event type
		if([_currentEvent triggeredBy: type] ||
			[_currentEvent triggeredBy: EVNT_QUIT]) {
			break;
		}
	}
}

@end //end of implementation
