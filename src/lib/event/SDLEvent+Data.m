/*
 * SDLEvent+Data.m
 * Implements a category that allows access to an event's data field
 * Created by Andrew Davis
 * Created on 9/10/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the category header
#import "SDLEvent+Data.h"

//category implementation
@implementation SDLEvent (Data)

//data method - returns the address of the data field
- (SDL_Event*) data {
	return &_data; //return the address of the data field
}

@end //end of implementation
