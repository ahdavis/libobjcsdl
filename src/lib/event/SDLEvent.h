/*
 * SDLEvent.h
 * Declares a class that represents an input event
 * Created by Andrew Davis
 * Created on 9/10/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "SDLEventType.h"
#import "../util/SDLKeycode.h"
#import <SDL2/SDL.h>

//class declaration
@interface SDLEvent : NSObject {
	//field
	SDL_Event _data; //the event data
}

//no properties

//method declarations

//initializes an SDLEvent instance
- (id) init;

//deallocates an SDLEvent instance
- (void) dealloc;

//returns whether the SDLEvent was triggered by a specific event type
- (bool) triggeredBy: (SDLEventType) eventType;

//returns the keycode for a key event
- (SDLKeycode) keycode;

@end //end of header
