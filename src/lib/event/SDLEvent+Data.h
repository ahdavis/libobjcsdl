/*
 * SDLEvent+Data.h
 * Declares a category that allows access to an event's data field
 * Created by Andrew Davis
 * Created on 9/10/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "SDLEvent.h"
#import <SDL2/SDL.h>

//category declaration
@interface SDLEvent (Data)

//method declaration

//returns the address of the data field
- (SDL_Event*) data;

@end //end of header
