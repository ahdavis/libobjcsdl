/*
 * SDLEvent.m
 * Implements a class that represents an input event
 * Created by Andrew Davis
 * Created on 9/10/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLEvent.h"

//class implementation
@implementation SDLEvent

//init method - initializes an SDLEvent instance
- (id) init {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then zero out the field
		memset(&_data, 0x00, sizeof(_data));
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLEvent instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//triggeredBy method - returns whether the event was triggered
//by a specific event type
- (bool) triggeredBy: (SDLEventType) eventType {
	//return whether the event types match
	return _data.type == eventType;
}

//keycode method - returns the keycode for the event
- (SDLKeycode) keycode {
	//return the keycode
	return _data.key.keysym.sym;
}

@end //end of implementation
