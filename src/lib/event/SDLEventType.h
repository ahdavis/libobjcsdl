/*
 * SDLEventType.h
 * Enumerates types of SDL events
 * Created by Andrew Davis
 * Created on 9/10/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include
#include <SDL2/SDL.h>

//enum definition
typedef enum {
	EVNT_WINDOW = SDL_WINDOWEVENT, //window event
	EVNT_KEYUP = SDL_KEYUP, //key released
	EVNT_KEYDOWN = SDL_KEYDOWN, //key pressed
	EVNT_MOUSEMOTION = SDL_MOUSEMOTION, //mouse motion event
	EVNT_MOUSEDOWN = SDL_MOUSEBUTTONDOWN, //mouse clicked
	EVNT_MOUSEUP = SDL_MOUSEBUTTONUP, //mouse released
	EVNT_QUIT = SDL_QUIT //window closed
} SDLEventType;

//end of header
