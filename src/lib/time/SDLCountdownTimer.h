/*
 * SDLCountdownTimer.h
 * Declares a class that represents a countdown timer
 * Created by Andrew Davis
 * Created on 12/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#include <SDL2/SDL.h>
#include <stdint.h>
#import "SDLTimerCallback.h"

//class declaration
@interface SDLCountdownTimer : NSObject {
	//fields
	uint32_t _interval; //the amount of time on the timer
	SDLTimerCallback _callback; //the timer's callback function
	NSMutableData* _data; //the data to be passed to the timer callback
	bool _started; //has the timer been started?
	SDL_TimerID _timerID; //the internal timer ID
}

//property declarations
@property (readwrite) uint32_t interval;
@property (readonly) bool started;

//method declarations

//initializes an SDLCountdownTimer instance from an NSString
- (id) initWithInterval: (uint32_t) newInterval
	andCallback: (SDLTimerCallback) newCallback
	andString: (NSString*) str;

//initializes an SDLCountdownTimer instance from raw data
- (id) initWithInterval: (uint32_t) newInterval
	andCallback: (SDLTimerCallback) newCallback
	andData: (NSMutableData*) newData;

//deallocates an SDLCountdownTimer instance
- (void) dealloc;

//starts the timer
- (void) start;

@end //end of header
