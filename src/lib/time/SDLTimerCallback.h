/*
 * SDLTimerCallback.h
 * Defines a type that represents a timer callback function
 * Created by Andrew Davis
 * Created on 12/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include
#include <stdint.h>

//typedef definition
typedef uint32_t (*SDLTimerCallback)(uint32_t, void*);

//end of header
