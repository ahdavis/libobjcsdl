/*
 * SDLTimer.h
 * Declares a class that represents a timer
 * Created by Andrew Davis
 * Created on 12/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLTimer : NSObject {
	//fields
	NSUInteger _startTicks; //the time when the timer was started
	NSUInteger _pausedTicks; //the ticks stored during pauses
	bool _paused; //is the timer paused?
	bool _started; //is the timer started?
}

//property declaration
@property (readonly) bool started;

//method declarations

//initializes an SDLTimer instance
- (id) init;

//deallocates an SDLTimer instance
- (void) dealloc;

//starts the timer
- (void) start;

//stops the timer
- (void) stop;

//pauses the timer
- (void) pause;

//resumes the timer after a pause
- (void) resume;

//returns the timer's time in milliseconds
- (NSUInteger) ticks;

//returns whether the timer is paused
- (bool) paused;

@end //end of header
