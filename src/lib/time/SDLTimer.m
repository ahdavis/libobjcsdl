/*
 * SDLTimer.m
 * Implements a class that represents a timer
 * Created by Andrew Davis
 * Created on 12/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLTimer.h"

//include the SDL library header
#include <SDL2/SDL.h>

//class implementation
@implementation SDLTimer

//property synthesis
@synthesize started = _started;

//init method - initializes an SDLTimer instance
- (id) init {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_startTicks = 0;
		_pausedTicks = 0;
		_paused = NO;
		_started = NO;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLTimer instance
- (void) dealloc {
	[self stop]; //stop the timer
	[super dealloc]; //and call the superclass dealloc method
}

//start method - starts the timer
- (void) start {
	_started = YES; //set the started flag
	_paused = NO; //clear the paused flag
	_startTicks = SDL_GetTicks(); //get the current clock time
	_pausedTicks = 0; //and zero the paused ticks
}

//stop method - stops the timer
- (void) stop {
	_started = NO; //clear the started flag
	_paused = NO; //clear the paused flag
	_startTicks = 0; //clear the start ticks
	_pausedTicks = 0; //clear the paused ticks
}

//pause method - pauses the timer
- (void) pause {
	if(_started && !_paused) { //if the timer is running
		_paused = YES; //then set the paused flag

		//calculate the paused ticks
		_pausedTicks = SDL_GetTicks() - _startTicks;

		//and clear the start ticks
		_startTicks = 0;
	}
}

//resume method - resumes the timer after being paused
- (void) resume {
	if(_started && _paused) { //if the timer is paused
		_paused = NO; //then clear the paused flag

		//calculate the starting ticks
		_startTicks = SDL_GetTicks() - _pausedTicks;

		//and clear the paused ticks
		_pausedTicks = 0;
	}
}

//ticks method - returns the timer's time in milliseconds
- (NSUInteger) ticks {
	NSUInteger ret = 0; //the return value

	//calculate the time
	if(_started) { //if the timer is running
		if(_paused) { //if the timer is paused
			//then return the number of ticks
			//when the timer was paused
			ret = _pausedTicks; 
		} else {
			//return the current time minus the start time
			ret = SDL_GetTicks() - _startTicks;
		}
	}

	//and return the calculated time
	return ret;
}

//paused method - returns whether the timer is paused
- (bool) paused {
	//return whether the timer is running and paused
	return _paused && _started; 
}

@end //end of implementation
