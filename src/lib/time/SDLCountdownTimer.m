/*
 * SDLCountdownTimer.m
 * Implements a class that represents a countdown timer
 * Created by Andrew Davis
 * Created on 12/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLCountdownTimer.h"

//import the SDLTimerException class header
#import "../except/SDLTimerException.h"

//class implementation
@implementation SDLCountdownTimer

//property synthesis
@synthesize interval = _interval;
@synthesize started = _started;

//first init method - initializes an SDLCountdownTimer instance
//from a string
- (id) initWithInterval: (uint32_t) newInterval
	andCallback: (SDLTimerCallback) newCallback
	  andString: (NSString*) str {
	//get the length of the string
	NSUInteger len = [str lengthOfBytesUsingEncoding:
				NSUTF8StringEncoding];

	//call the other init method
	return [self initWithInterval: newInterval
			andCallback: newCallback
			    andData: [[NSMutableData alloc] 
		      			initWithBytes: [str UTF8String]
					       length: len + 1]];
}

//second init method - initializes an SDLCountdownTimer instance
//from raw data
- (id) initWithInterval: (uint32_t) newInterval
	andCallback: (SDLTimerCallback) newCallback
	    andData: (NSMutableData*) newData {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_interval = newInterval;
		_callback = newCallback;
		_data = newData;
		[_data retain];
		_started = NO;
		_timerID = 0;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLCountdownTimer instance
- (void) dealloc {
	//remove the timer if it was started
	if(_timerID != 0) {
		SDL_RemoveTimer(_timerID);
		_timerID = 0;
	}

	//release the data
	[_data release];

	//and call the superclass dealloc method
	[super dealloc];
}

//start method - starts the timer
- (void) start {
	//add the timer
	_timerID = SDL_AddTimer(_interval, _callback, 
					[_data mutableBytes]);

	//verify that the add succeeded
	if(_timerID == 0) { //if the add failed
		//then throw an exception
		@throw [SDLTimerException exception];
	}

	//and set the started flag
	_started = YES;
}

@end //end of implementation
