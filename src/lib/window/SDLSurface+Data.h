/*
 * SDLSurface+Data.h
 * Declares a category that allows access to an SDLSurface's data pointer
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#include <SDL2/SDL.h>
#import <Foundation/Foundation.h>
#import "SDLSurface.h"

//category declaration
@interface SDLSurface (Data)

//method declaration

//returns the data pointer
- (SDL_Surface*) data;

//sets the data pointer
- (void) setData: (SDL_Surface*) newData;

@end //end of header
