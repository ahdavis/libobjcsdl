/*
 * SDLSurface.m
 * Implements a class that represents a window surface
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLSurface.h"

//import the SDLWindow+Data category header
#import "SDLWindow+Data.h"

//import the SDLExtException class header
#import "../except/SDLExtException.h"

//import the SDLImgException class header
#import "../except/SDLImgException.h"

//import the SDLRect+Data category header
#import "../util/SDLRect+Data.h"

//import the SDLScaleException class header
#import "../except/SDLScaleException.h"

//import the SDLBlitException class header
#import "../except/SDLBlitException.h"

//import the SDL_image library header
#import <SDL2/SDL_image.h>

//import the SDLFont+Data category header
#import "../element/SDLFont+Data.h"

//import the SDL_ttf library header
#import <SDL2/SDL_ttf.h>

//import the SDLColor+Data category header
#import "../util/SDLColor+Data.h"

//import the SDLTextException class header
#import "../except/SDLTextException.h"

//class implementation
@implementation SDLSurface

//property synthesis
@synthesize isWindowSurface = _isWindowSurface;

//first init method - initializes an SDLSurface instance from a window
- (id) initWithWindow: (SDLWindow*) window {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_data = SDL_GetWindowSurface([window data]);
		_isWindowSurface = YES;
	}

	//and return the instance
	return self;
}

//second init method - initializes an SDLSurface instance from
//an image path
- (id) initWithImagePath: (NSString*) imagePath {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_data = NULL;

		//get the extension of the image path
		NSString* ext = [imagePath pathExtension];

		//make sure that the extension is correct
		if([ext isEqualToString: @"bmp"]) {
			//BMP file
			//load the file data
			_data = SDL_LoadBMP([imagePath UTF8String]);

			//handle image load failure
			if(_data == NULL) {
				@throw [SDLImgException
				exceptionWithPath: imagePath
					    isBMP: YES];
			}
		} else if([ext isEqualToString: @"png"]) {
			//PNG file
			//load the file data
			_data = IMG_Load([imagePath UTF8String]);

			//handle image load failure
			if(_data == NULL) {
				@throw [SDLImgException
				exceptionWithPath: imagePath
					    isBMP: NO];
			}
		} else {
			//invalid file type, so throw an exception
			@throw [SDLExtException 
					exceptionWithPath: imagePath];
		}

		//clear the window flag
		_isWindowSurface = NO;
	}

	//and return the instance
	return self;
}

//third init method - initializes an SDLSurface instance
//from text, a font, and a text color
- (id) initWithText: (NSString*) text andFont: (SDLFont*) font
	withColor: (SDLColor*) color {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the data field
		_data = TTF_RenderText_Solid([font data],
						[text UTF8String],
						[color data]);
		//verify that initialization succeeded
		if(_data == NULL) { //if initialization failed
			//then throw an exception
			@throw [SDLTextException exceptionWithText: text];
		}

		//clear the window flag
		_isWindowSurface = NO;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLSurface instance
- (void) dealloc {
	//determine whether to destroy the surface data
	if(!_isWindowSurface) {
		SDL_FreeSurface(_data);
		_data = NULL;
	}
	[super dealloc]; //and call the superclass dealloc method
}

//width method - returns the width of the surface
- (int) width {
	return _data->w; //return the width of the surface
}

//height method - returns the height of the surface
- (int) height {
	return _data->h; //return the height of the surface
}

//fill method - fills the SDLSurface with a specified color
- (void) fillWithColor: (SDLColor*) color {
	//fill the surface with the color
	SDL_FillRect(_data, NULL, SDL_MapRGBA(_data->format,
					[color red], 
					[color green], 
					[color blue], 
					[color alpha])); 
}

//blitFromSource method - blits a graphics element onto the SDLSurface
- (void) blitFromSource: (SDLElement*) source {
	//attempt to blit the element onto the surface
	int res = SDL_BlitSurface([source surface]->_data, 
					NULL, _data, NULL);

	//and verify that the blit succeeded
	if(res < 0) { //if the blit failed
		//then throw an exception
		@throw [SDLBlitException exception];
	}
}

//blitFromSource:withTarget: method - blits a graphics element
//onto the SDLSurface with target scaling
- (void) blitFromSource: (SDLElement*) source
	withTarget: (SDLRect*) target {
	//get a copy of the target data
	SDL_Rect targetData = [target data];

	//attempt to blit the element onto the surface
	int res = SDL_BlitScaled([source surface]->_data, NULL, _data,
			&targetData);

	//and make sure that the blit was successful
	if(res < 0) { //if the blit failed
		//then throw an exception
		@throw [SDLScaleException exception];
	}
}

//blitFromSource:withBounds:andTarget: method - blits
//a section of an image element onto the surface
- (void) blitFromSource: (SDLElement*) source 
	withBounds: (SDLRect*) bounds andTarget: (SDLRect*) target {
	//get copies of the bounds and target data
	SDL_Rect boundsData = [bounds data];
	SDL_Rect targetData = [target data];

	//attempt to blit the element onto the target
	int res = SDL_BlitScaled([source surface]->_data,
					&boundsData,
					_data,
					&targetData);

	//and verify that the blit succeeded
	if(res < 0) { //if the blit failed
		//then throw an exception
		@throw [SDLScaleException exception];
	}
}

@end //end of implementation
