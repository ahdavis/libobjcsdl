/*
 * SDLWindow.m
 * Implements a class that represents a window
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLWindow.h"

//import the SDLWindowException header
#import "../except/SDLWindowException.h"

//class implementation
@implementation SDLWindow

//property synthesis
@synthesize title = _title;
@synthesize xPos = _xPos;
@synthesize yPos = _yPos;
@synthesize width = _width;
@synthesize height = _height;

//first init method - initializes an SDLWindow instance 
//with a programmer-defined position
- (id) initWithTitle: (NSString*) newTitle andXPos: (int) newXPos
	andYPos: (int) newYPos andWidth: (int) newWidth
      andHeight: (int) newHeight {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then attempt to init the data field
		_data = SDL_CreateWindow([newTitle UTF8String],
					newXPos,
					newYPos,
					newWidth,
					newHeight,
					SDL_WINDOW_SHOWN);
		//verify that the data field initialized successfully
		if(_data == NULL) { //if the init failed
			//then throw an exception
			@throw [SDLWindowException 
				exceptionWithTitle: newTitle];
		}

		//and init the other fields
		_title = newTitle;
		[_title retain];
		_xPos = newXPos;
		_yPos = newYPos;
		_width = newWidth;
		_height = newHeight;
	}

	//and return the instance
	return self;
}

//second init method - initializes an SDLWindow instance
//with an OS-defined position
- (id) initWithTitle: (NSString*) newTitle andWidth: (int) newWidth
	andHeight: (int) newHeight {
	//call the other init method
	return [self initWithTitle: newTitle 
		andXPos: SDL_WINDOWPOS_UNDEFINED
		andYPos: SDL_WINDOWPOS_UNDEFINED
	       andWidth: newWidth
	      andHeight: newHeight];
}

//dealloc method - deallocates an SDLWindow instance
- (void) dealloc {
	SDL_DestroyWindow(_data); //destroy the window pointer
	_data = NULL; //and zero it out
	[_title release]; //release the title field
	[super dealloc]; //and call the superclass dealloc method
}

//update method - updates the window
- (void) update {
	SDL_UpdateWindowSurface(_data); //update the window
}

@end //end of implementation
