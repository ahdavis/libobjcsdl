/*
 * SDLWindow.h
 * Declares a class that represents a window
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#include <SDL2/SDL.h>
#import <Foundation/Foundation.h>

//class declaration
@interface SDLWindow : NSObject {
	//fields
	SDL_Window* _data; //the window data
	NSString* _title; //the window title
	int _xPos; //the x-position of the window
	int _yPos; //the y-position of the window
	int _width; //the width of the window
	int _height; //the height of the window
}

//property declarations
@property (nonatomic, readonly) NSString* title;
@property (readonly) int xPos;
@property (readonly) int yPos;
@property (readonly) int width;
@property (readonly) int height;

//method declarations

//initializes an SDLWindow instance with a programmer-defined position
//throws an SDLWindowException if the initialization fails
- (id) initWithTitle: (NSString*) newTitle andXPos: (int) newXPos
	andYPos: (int) newYPos andWidth: (int) newWidth 
	andHeight: (int) newHeight;

//initializes an SDLWindow instance with an OS-defined position
//throws an SDLWindowException if the initialization fails
- (id) initWithTitle: (NSString*) newTitle andWidth: (int) newWidth
	andHeight: (int) newHeight;

//deallocates an SDLWindow instance
- (void) dealloc;

//updates the window
- (void) update;

@end //end of header
