/*
 * SDLSurface.h
 * Declares a class that represents a window surface
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#include <SDL2/SDL.h>
#import <Foundation/Foundation.h>
#import "SDLWindow.h"
#import "../util/SDLColor.h"
#import "../element/SDLElement.h"
#import "../util/SDLRect.h"
#import "../element/SDLFont.h"

//class declaration
@interface SDLSurface : NSObject {
	//fields
	SDL_Surface* _data; //the surface data
	bool _isWindowSurface; //is the Surface a window surface?
}

//property declaration
@property (readonly) bool isWindowSurface;

//method declarations

//initializes an SDLSurface instance from a window
- (id) initWithWindow: (SDLWindow*) window;

//initializes an SDLSurface instance from an image file path
- (id) initWithImagePath: (NSString*) imagePath;

//initializes an SDLSurface instance from a text string,
//a font, and a text color
- (id) initWithText: (NSString*) text andFont: (SDLFont*) font
	withColor: (SDLColor*) color;

//deallocates an SDLSurface instance
- (void) dealloc;

//returns the width of the surface
- (int) width;

//returns the height of the surface
- (int) height;

//fills the SDLSurface with a specified color
- (void) fillWithColor: (SDLColor*) color;

//blits a graphics element onto the SDLSurface
- (void) blitFromSource: (SDLElement*) source;

//blits a graphics element onto the SDLSurface with only target scaling
- (void) blitFromSource: (SDLElement*) source 
	withTarget: (SDLRect*) target;

//blits a graphics element onto the SDLSurface with both source
//and target scaling
- (void) blitFromSource: (SDLElement*) source
	withBounds: (SDLRect*) bounds andTarget: (SDLRect*) target;

@end //end of header
