/*
 * SDLWindow+Data.h
 * Declares a category that allows access to an SDLWindow's data pointer
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#include <SDL2/SDL.h>
#import "SDLWindow.h"
#import <Foundation/Foundation.h>

//category declaration
@interface SDLWindow (Data)

//method declaration

//returns the SDLWindow's data pointer
- (SDL_Window*) data;

@end //end of header
