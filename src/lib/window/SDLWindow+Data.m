/*
 * SDLWindow+Data.m
 * Implements a category that allows access to an SDLWindow's data pointer
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the category header
#import "SDLWindow+Data.h"

//category implementation
@implementation SDLWindow (Data)

//data method - returns the SDLWindow's data pointer
- (SDL_Window*) data {
	return _data; //return the data pointer
}

@end //end of implementation
