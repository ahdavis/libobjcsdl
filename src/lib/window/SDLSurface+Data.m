/*
 * SDLSurface+Data.m
 * Implements a category that allows access to an SDLSurface's data pointer
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the category header
#import "SDLSurface+Data.h"

//category implementation
@implementation SDLSurface (Data)

//data method - returns the SDLSurface's data field
- (SDL_Surface*) data {
	return _data; //return the data field
}

//setData method - sets the SDLSurface's data field
- (void) setData: (SDL_Surface*) newData {
	_data = newData; //set the data field
}

@end //end of implementation
