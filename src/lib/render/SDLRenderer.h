/*
 * SDLRenderer.h
 * Declares a class that represents a hardware renderer
 * Created by Andrew Davis
 * Created on 9/16/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <SDL2/SDL.h>
#import "../window/SDLWindow.h"
#import "../util/SDLColor.h"
#import "../util/SDLCircle.h"
#import "../util/SDLPoint.h"
#import "../util/SDLRect.h"
#import "SDLSpriteSheet.h"
#import "SDLAnimation.h"
#import "SDLFlipType.h"

//forward declare the SDLTexture class
@class SDLTexture;

//class declaration
@interface SDLRenderer : NSObject {
	//fields
	SDL_Renderer* _data; //the hardware renderer interface
	SDLColor* _drawColor; //the current draw color
}

//property declaration
@property (nonatomic, retain) SDLColor* drawColor;

//method declarations

//initializes an SDLRenderer instance with a white draw color
- (id) initWithWindow: (SDLWindow*) window;

//initializes an SDLRenderer instance with
//a programmer specified draw color
- (id) initWithWindow: (SDLWindow*) window 
	andDrawColor: (SDLColor*) newDrawColor;

//deallocates an SDLRenderer instance
- (void) dealloc;

//sets the draw color of the renderer
- (void) setDrawColor: (SDLColor*) color;

//refreshes the rendered screen
- (void) refreshScreen;

//clears the rendered screen
- (void) clearScreen;

//sets the renderer's viewport
- (void) setViewport: (SDLRect*) viewport;

//removes the renderer's viewport
- (void) removeViewport;

//renders a texture at (0, 0)
- (void) renderTexture: (SDLTexture*) texture;

//renders a texture at programmer-specified coordinates
- (void) renderTexture: (SDLTexture*) texture atX: (int) x andY: (int) y;

//renders a texture at programmer-specified coordinates
//with clipping and positioning
- (void) renderTexture: (SDLTexture*) texture withClip: (SDLRect*) clip
	atX: (int) x andY: (int) y;

//renders a texture at programmer-specified coordinates
//with clipping, positioning, and rotation
- (void) renderTexture: (SDLTexture*) texture withClip: (SDLRect*) clip
	atX: (int) x andY: (int) y atAngle: (double) angle;

//renders a texture at programmer-specified coordinates
//with clipping, positioning, rotation, and flipping
- (void) renderTexture: (SDLTexture*) texture withClip: (SDLRect*) clip
	atX: (int) x andY: (int) y atAngle: (double) angle
	flippedWith: (SDLFlipType) flip centeredOn: (SDLPoint*) center;

//renders a texture stored in a spritesheet
- (void) renderTextureFromSpriteSheet: (SDLSpriteSheet*) sheet
	withXIndex: (int) xIndex andYIndex: (int) yIndex
	atX: (int) x andY: (int) y;

//renders an animation at (0, 0)
- (void) renderAnimation: (SDLAnimation*) animation;

//renders an animation at programmer-specified coordinates
- (void) renderAnimation: (SDLAnimation*) animation
	atX: (int) x andY: (int) y;

//renders an empty rectangle
- (void) renderEmptyRect: (SDLRect*) rect;

//renders a filled rectangle
- (void) renderFilledRect: (SDLRect*) rect;

//renders an empty circle
- (void) renderEmptyCircle: (SDLCircle*) circle;

//renders a filled circle
- (void) renderFilledCircle: (SDLCircle*) circle;

//renders a line
- (void) renderLineFromPointA: (SDLPoint*) pointA 
	toPointB: (SDLPoint*) pointB;

//renders a point
- (void) renderPoint: (SDLPoint*) point;

@end //end of header
