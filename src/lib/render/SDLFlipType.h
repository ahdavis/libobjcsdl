/*
 * SDLFlipType.h
 * Enumerates texture flipping flags
 * Created on 9/27/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include
#include <SDL2/SDL.h>

//enum definition
typedef enum {
	FLIP_NONE = SDL_FLIP_NONE,
	FLIP_HORIZ = SDL_FLIP_HORIZONTAL,
	FLIP_VERT = SDL_FLIP_VERTICAL
} SDLFlipType;

//end of enum
