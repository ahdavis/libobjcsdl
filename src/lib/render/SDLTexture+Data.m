/*
 * SDLTexture+Data.m
 * Implements a category that allows access to an SDLTexture's data field
 * Created by Andrew Davis
 * Created on 9/14/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the category header
#import "SDLTexture+Data.h"

//category implementation
@implementation SDLTexture (Data)

//data method - returns the data field
- (SDL_Texture*) data {
	return _data; //return the data field
}

@end //end of implementation
