/*
 * SDLAnimation+NextFrame.h
 * Declares a category that adds a nextFrame method to SDLAnimation
 * Created on 10/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import "SDLAnimation.h"

//category declaration
@interface SDLAnimation (NextFrame)

//moves the animation to the next frame
- (void) nextFrame;

@end //end of header
