/*
 * SDLRenderer.m
 * Implements a class that represents a hardware renderer
 * Created by Andrew Davis
 * Created on 9/16/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLRenderer.h"

//import the SDLTexture class header
#import "SDLTexture.h"

//import the SDLTexture+Data category header
#import "SDLTexture+Data.h"

//import the SDLRendererException class header
#import "../except/SDLRendererException.h"

//import the SDLWindow+Data category header
#import "../window/SDLWindow+Data.h"

//import the SDLRect+Data category header
#import "../util/SDLRect+Data.h"

//import the math library header
#import <math.h>

//import the SDLSurface class header
#import "../window/SDLSurface.h"

//import the SDLPoint+Data category header
#import "../util/SDLPoint+Data.h"

//import the SDLAnimation+NextFrame category header
#import "SDLAnimation+NextFrame.h"

//class implementation
@implementation SDLRenderer

//property synthesis
@synthesize drawColor = _drawColor;

//first init method - initializes an SDLRenderer instance
//with white as the draw color
- (id) initWithWindow: (SDLWindow*) window {
	//call the second init method
	return [self initWithWindow: window 
		       andDrawColor: [[SDLColor alloc] initWithRed: 0xFF
		       			andGreen: 0xFF
					 andBlue: 0xFF
					andAlpha: 0xFF]];
}

//second init method - initializes an SDLRenderer instance
//with a programmer-specified draw color
- (id) initWithWindow: (SDLWindow*) window 
	andDrawColor: (SDLColor*) newDrawColor {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the draw color
		_drawColor = nil;
		[self setDrawColor: newDrawColor];

		//init the data pointer
		_data = SDL_CreateRenderer([window data], -1,
						SDL_RENDERER_ACCELERATED |
						SDL_RENDERER_PRESENTVSYNC);

		//and make sure that initialization succeeded
		if(_data == NULL) { //if initialization failed
			//then throw an exception
			@throw [SDLRendererException exception];
		}
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLRenderer instance
- (void) dealloc {
	SDL_DestroyRenderer(_data); //destroy the data pointer
	_data = NULL; //and zero it out
	[_drawColor release]; //release the draw color
	[super dealloc]; //and call the superclass dealloc method
}

//setDrawColor method - sets the draw color for the SDLRenderer
- (void) setDrawColor: (SDLColor*) color {
	//set the renderer's draw color
	SDL_SetRenderDrawColor(_data, [color red], [color green],
					[color blue], [color alpha]);

	//and update the draw color field
	[_drawColor release];
	_drawColor = color;
	[_drawColor retain];
}

//refreshScreen method - refreshes the renderer's screen
- (void) refreshScreen {
	SDL_RenderPresent(_data); //refresh the screen
}

//clearScreen method - clears the renderer's screen
- (void) clearScreen {
	SDL_RenderClear(_data); //clear the screen
}

//setViewport method - sets the renderer's viewport
- (void) setViewport: (SDLRect*) viewport {
	//get the argument's data field
	SDL_Rect rectData = [viewport data];

	//and set the viewport
	SDL_RenderSetViewport(_data, &rectData);
}

//removeViewport method - removes the renderer's viewport
- (void) removeViewport {
	//remove the viewport
	SDL_RenderSetViewport(_data, NULL);
}

//first renderTexture method - renders a texture at (0, 0)
- (void) renderTexture: (SDLTexture*) texture {
	//render the texture to the screen
	SDL_RenderCopy(_data, [texture data], NULL, NULL);
}

//second renderTexture method - renders a texture 
//at programmer-specified coordinates
- (void) renderTexture: (SDLTexture*) texture 
	atX: (int) x andY: (int) y {
	//get a rectangle containing the bounds of the texture
	SDLRect* target = [[SDLRect alloc] 
				initWithX: x 
				     andY: y
				 andWidth: [[[texture element] 
				 		surface] width]
				andHeight: [[[texture element]
						surface] height]];

	//get the data from the rectangle
	SDL_Rect targetData = [target data];

	//render the texture
	SDL_RenderCopy(_data, [texture data], NULL, &targetData);

	//and release the bounding rectangle
	[target release];
}

//third renderTexture method - renders a texture 
//with clipping and positioning
- (void) renderTexture: (SDLTexture*) texture withClip: (SDLRect*) clip
	atX: (int) x andY: (int) y {
	//get a positioning rectangle
	SDL_Rect pos = {x, y, [clip width], [clip height]};

	//get the data of the clip rectangle
	SDL_Rect clipData = [clip data];

	//and render the texture
	SDL_RenderCopy(_data, [texture data], &clipData, &pos);
}

//fourth renderTexture method - renders a texture
//with clipping, positioning, and rotation
- (void) renderTexture: (SDLTexture*) texture withClip: (SDLRect*) clip
	atX: (int) x andY: (int) y atAngle: (double) angle {
	//call the fifth renderTexture method
	[self renderTexture: texture withClip: clip atX: x andY: y
		    atAngle: angle flippedWith: FLIP_NONE centeredOn: nil];
}

//fifth renderTexture method - renders a texture
//with clipping, positioning, rotation, and flipping
- (void) renderTexture: (SDLTexture*) texture withClip: (SDLRect*) clip
	atX: (int) x andY: (int) y atAngle: (double) angle
	flippedWith: (SDLFlipType) flip centeredOn: (SDLPoint*) center {
	//get the data from the SDLRect and SDLPoint arguments
	SDL_Rect tmpClipData = [clip data];
	SDL_Point tmpCenData = [center data];
	SDL_Rect* clipData = NULL;
	SDL_Point* cenData = NULL;
	if(clip != nil) {
		clipData = &tmpClipData;
	}
	if(center != nil) {
		cenData = &tmpCenData;
	}

	//get the position data
	int texWidth = texture.element.surface.width;
	int texHeight = texture.element.surface.height;
	SDL_Rect pos = {x, y, texWidth, texHeight};
	if(clip != nil) {
		pos.w = [clip width];
		pos.h = [clip height];
	}

	//convert the flip argument
	SDL_RendererFlip cFlip = (SDL_RendererFlip)flip;

	//render the texture
	SDL_RenderCopyEx(_data, [texture data], clipData, &pos,
				angle, cenData, cFlip);
	
}

//sixth renderTexture method - renders a texture stored in a spritesheet
- (void) renderTextureFromSpriteSheet: (SDLSpriteSheet*) sheet
	withXIndex: (int) xIndex andYIndex: (int) yIndex
	       atX: (int) x andY: (int) y {
	//get the bounds of the indexed sprite
	SDLRect* bounds = [sheet boundsForSpriteWithXIndex: xIndex
				andYIndex: yIndex];
	
	//and call the third renderTexture method
	[self renderTexture: [sheet texture] withClip: bounds
			atX: x andY: y];

}

//first renderAnimation method - renders an animation at (0, 0)
- (void) renderAnimation: (SDLAnimation*) animation {
	//call the second renderAnimation method
	[self renderAnimation: animation atX: 0 andY: 0];
}

//second renderAnimation method - renders an animation at
//programmer-specified coordinates
- (void) renderAnimation: (SDLAnimation*) animation
	atX: (int) x andY: (int) y {
	//call the third renderTexture method
	[self renderTexture: [[animation sprites] texture]
		   withClip: [animation boundsForCurrentFrame]
			atX: x andY: y];

	//and move the animation to the next frame
	[animation nextFrame];
}

//renderEmptyRect method - renders an empty rectangle
- (void) renderEmptyRect: (SDLRect*) rect {
	//get the data from the argument
	SDL_Rect rectData = [rect data];

	//and render the empty rectangle
	SDL_RenderDrawRect(_data, &rectData);
}

//renderFilledRect method - renders a filled rectangle
- (void) renderFilledRect: (SDLRect*) rect {
	//get the data from the argument
	SDL_Rect rectData = [rect data];

	//and render the filled rectangle
	SDL_RenderFillRect(_data, &rectData);
}

//renderEmptyCircle method - renders an empty circle
- (void) renderEmptyCircle: (SDLCircle*) circle {
	//loop through 360 degrees
	int deg;
	for(deg = 0; deg < 360; deg++) {
		//render each point on the circle
		SDLPoint* curPoint = [[SDLPoint alloc] 
			initWithX: 
			[[circle origin] x] + 
				([circle radius] * 
				 cos(deg * (M_PI / 180)))
				     andY: 
			[[circle origin] y] + 
				([circle radius] * 
				 sin(deg * (M_PI / 180)))];
		[self renderPoint: curPoint];
		[curPoint release];
	}
}

//renderFilledCircle method - renders a filled circle
- (void) renderFilledCircle: (SDLCircle*) circle {
	//loop through 360 degrees
	int deg;
	for(deg = 0; deg < 360; deg++) {
		//get the current point on the outside of the circle
		SDLPoint* curPoint = [[SDLPoint alloc] 
			initWithX: 
			[[circle origin] x] + 
				([circle radius] * 
				 cos(deg * (M_PI / 180)))
			andY:
			[[circle origin] y] +
				([circle radius] *
				 sin(deg * (M_PI / 180)))];

		//render a line from the origin to the point
		[self renderLineFromPointA: [circle origin]
				  toPointB: curPoint];

		//and release the point
		[curPoint release];
	}
}

//renderLine method - renders a line between 2 points
- (void) renderLineFromPointA: (SDLPoint*) pointA 
	toPointB: (SDLPoint*) pointB {
	//render the line
	SDL_RenderDrawLine(_data, [pointA x], [pointA y],
				[pointB x], [pointB y]);
}

//renderPoint method - renders a point
- (void) renderPoint: (SDLPoint*) point {
	//render the point
	SDL_RenderDrawPoint(_data, [point x], [point y]);
}

@end //end of implementation
