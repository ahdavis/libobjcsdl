/*
 * SDLTexture+Data.h
 * Declares a category that allows access to an SDLTexture's data field
 * Created by Andrew Davis
 * Created on 9/14/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <SDL2/SDL.h>
#import "SDLTexture.h"

//category declaration
@interface SDLTexture (Data)

//returns the data field
- (SDL_Texture*) data;

@end //end of header
