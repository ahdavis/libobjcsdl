/*
 * SDLTexture.m
 * Implements a class that represents a hardware texture
 * Created by Andrew Davis
 * Created on 9/16/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLTexture.h"

//import the SDLRenderer+Data category header
#import "SDLRenderer+Data.h"

//import the SDLTextureException class header
#import "../except/SDLTextureException.h"

//import the SDLSurface+Data category header
#import "../window/SDLSurface+Data.h"

//class implementation
@implementation SDLTexture

//property synthesis
@synthesize element = _element;

//first init method - initializes an SDLTexture element from an image
- (id) initWithImage: (SDLImgElement*) image 
	andRenderer: (SDLRenderer*) renderer {
	//call the superclass init method
	self = [super init];

	//verify that the call was successful
	if(self) { //if the call was successful
		//then init the element field
		_element = image;
		[_element retain];

		//initialize the texture data pointer
		_data = SDL_CreateTextureFromSurface([renderer data],
					[[_element surface] data]);

		//and make sure that the initialization succeeded
		if(_data == NULL) { //if initialization failed
			//then throw an exception
			@throw [SDLTextureException 
				exceptionWithImagePath: 
					[image imagePath]];
		}
	}

	//and return the instance
	return self;
}

//second init method - initializes an SDLTexture instance
//from an SDLTextElement instance
- (id) initWithText: (SDLTextElement*) text 
	andRenderer: (SDLRenderer*) renderer {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the element field
		_element = text;
		[_element retain];

		//initialize the texture data pointer
		_data = SDL_CreateTextureFromSurface([renderer data],
						[[_element surface] data]);

		//and make sure that initialization succeeded
		if(_data == NULL) { //if initialization failed
			//then throw an exception
			@throw [SDLTextureException exceptionWithText:
					[text text]];
		}
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLTexture instance
- (void) dealloc {
	SDL_DestroyTexture(_data); //destroy the texture pointer
	_data = NULL; //and zero it out
	[_element release]; //release the element field
	[super dealloc]; //and call the superclass dealloc method
}

//modulateWithColor method - modulates an SDLTexture instance's colors
- (void) modulateWithColor: (SDLColor*) color {
	//modulate the color of the texture
	SDL_SetTextureColorMod(_data, [color red], [color green],
				[color blue]);
}

//modulateWithAlpha method - sets the texture's alpha value
- (void) modulateWithAlpha: (uint8_t) alpha {
	//modulate the alpha value of the texture
	SDL_SetTextureAlphaMod(_data, alpha);
}

//enableAlphaModulation method - enables alpha modulation 
//for the texture
- (void) enableAlphaModulation {
	SDL_SetTextureBlendMode(_data, SDL_BLENDMODE_BLEND);
}

//disableAlphaModulation method - disables alpha modulation
//for the texture
- (void) disableAlphaModulation {
	SDL_SetTextureBlendMode(_data, SDL_BLENDMODE_NONE);
}

@end //end of implementation
