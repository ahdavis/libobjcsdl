/*
 * SDLSpriteSheet.m
 * Implements a class that represents a spritesheet
 * Created by Andrew Davis
 * Created on 9/18/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLSpriteSheet.h"

//import the SDLTexture class header
#import "SDLTexture.h"

//class implementation
@implementation SDLSpriteSheet

//property synthesis
@synthesize texture = _texture;
@synthesize spriteWidth = _spriteWidth;
@synthesize spriteHeight = _spriteHeight;

//init method - initializes an SDLSpriteSheet instance
- (id) initWithTexture: (SDLTexture*) newTexture
	andSpriteWidth: (int) width andSpriteHeight: (int) height {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_texture = newTexture;
		[_texture retain];
		_spriteWidth = width;
		_spriteHeight = height;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLSpriteSheet instance
- (void) dealloc {
	[_texture release]; //release the texture field
	[super dealloc]; //and call the superclass dealloc method
}

//boundsForSpriteWithXIndex:andYIndex: method - returns an autoreleased
//rectangle containing the bounds for an indexed sprite
- (SDLRect*) boundsForSpriteWithXIndex: (int) x andYIndex: (int) y {
	//get the pixel position of the indexed sprite
	int px = _spriteWidth * x;
	int py = _spriteHeight * y;

	//and return an autoreleased rectangle containing the
	//bounds of the sprite
	return [[[SDLRect alloc] initWithX: px andY: py
			andWidth: _spriteWidth 
		       andHeight: _spriteHeight] autorelease];
}

@end //end of implementation
