/*
 * SDLAnimation.m
 * Implements a class that represents a multiframe animation
 * Created by Andrew Davis
 * Created on 9/25/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLAnimation.h"

//class implementation
@implementation SDLAnimation

//property synthesis
@synthesize sprites = _sprites;
@synthesize currentFrame = _currentFrame;
@synthesize refreshRate = _refreshRate;
@synthesize frameCount = _frameCount;

//init method - initializes an SDLAnimation instance
- (id) initWithSpriteSheet: (SDLSpriteSheet*) newSprites
	andFrameCount: (int) newFrameCount
       andRefreshRate: (int) newRefreshRate {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_sprites = newSprites;
		[_sprites retain];
		_currentFrame = 0;
		_frameCount = newFrameCount;
		_refreshRate = newRefreshRate;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLAnimation instance
- (void) dealloc {
	[_sprites release]; //release the spritesheet field
	[super dealloc]; //and call the superclass dealloc method
}

//boundsForCurrentFrame method - returns the bounds of the
//sprite for the current frame
- (SDLRect*) boundsForCurrentFrame {
	//return the bounds for the current frame
	return [_sprites boundsForSpriteWithXIndex: 
		_currentFrame / _refreshRate
		andYIndex: 0];

}

@end //end of implementation
