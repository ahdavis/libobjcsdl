/*
 * SDLAnimation.h
 * Declares a class that represents a multiframe animation
 * Created by Andrew Davis
 * Created on 9/25/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "SDLSpriteSheet.h"
#import "../util/SDLRect.h"

//class declaration
@interface SDLAnimation : NSObject {
	//fields
	SDLSpriteSheet* _sprites; //the sprites that compose the animation
	int _frameCount; //the number of frames in the animation
	int _refreshRate; //the refresh rate of the animation
	int _currentFrame; //the current frame of the animation
}

//property declarations
@property (readonly) SDLSpriteSheet* sprites;
@property (readonly) int currentFrame;
@property (readonly) int frameCount;
@property (readwrite) int refreshRate;

//method declarations

//initializes an SDLAnimation instance
- (id) initWithSpriteSheet: (SDLSpriteSheet*) newSprites
	andFrameCount: (int) newFrameCount 
	andRefreshRate: (int) newRefreshRate;

//deallocates an SDLAnimation instance
- (void) dealloc;

//returns the bounds of the sprite for the current frame
- (SDLRect*) boundsForCurrentFrame;

@end //end of header
