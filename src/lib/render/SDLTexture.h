/*
 * SDLTexture.h
 * Declares a class that represents a hardware texture
 * Created by Andrew Davis
 * Created on 9/16/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <SDL2/SDL.h>
#import "SDLRenderer.h"
#import "../element/SDLElement.h"
#import "../element/SDLImgElement.h"
#import "../element/SDLTextElement.h"
#import "../util/SDLColor.h"

//class declaration
@interface SDLTexture : NSObject {
	//fields
	SDL_Texture* _data; //the hardware texture interface
	SDLElement* _element; //the SDLElement object for the texture
}

//property declaration
@property (readonly) SDLElement* element;

//method declarations

//initializes an SDLTexture instance from an SDLImgElement instance
- (id) initWithImage: (SDLImgElement*) image 
	andRenderer: (SDLRenderer*) renderer;

//initializes an SDLTexture instance from an SDLTextElement instance
- (id) initWithText: (SDLTextElement*) text
	andRenderer: (SDLRenderer*) renderer;

//deallocates an SDLTexture instance
- (void) dealloc;

//modulates an SDLTexture instance's colors
- (void) modulateWithColor: (SDLColor*) color;

//modulates an SDLTexture instance's alpha value
- (void) modulateWithAlpha: (uint8_t) alpha;

//enables alpha modulation for the texture
- (void) enableAlphaModulation;

//disables alpha modulation for the texture
- (void) disableAlphaModulation;

@end //end of header
