/*
 * SDLSpriteSheet.h
 * Declares a class that represents a spritesheet
 * Created by Andrew Davis
 * Created on 9/18/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "../util/SDLRect.h"

//forward declare the SDLTexture class
@class SDLTexture;

//class declaration
@interface SDLSpriteSheet : NSObject {
	//fields
	SDLTexture* _texture; //the texture of the spritesheet
	int _spriteWidth; //the width of each sprite 
	int _spriteHeight; //the height of each sprite
}

//property declarations
@property (readonly) SDLTexture* texture;
@property (readonly) int spriteWidth;
@property (readonly) int spriteHeight;

//method declarations

//initializes an SDLSpriteSheet instance
- (id) initWithTexture: (SDLTexture*) newTexture
	andSpriteWidth: (int) width andSpriteHeight: (int) height;

//deallocates an SDLSpriteSheet instance
- (void) dealloc;

//returns an autoreleased rectangle containing the bounds
//of a sprite in terms of its pixel position
//the sprite is indexed according to its position
//on the sheet, not its pixel position
- (SDLRect*) boundsForSpriteWithXIndex: (int) x andYIndex: (int) y;

@end //end of header
