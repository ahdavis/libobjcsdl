/*
 * SDLAnimation+NextFrame.m
 * Implements a category that adds a nextFrame method to SDLAnimation
 * Created on 10/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the category header
#import "SDLAnimation+NextFrame.h"

//category implementation
@implementation SDLAnimation (NextFrame)

//nextFrame method - moves the animation to its next frame
- (void) nextFrame {
	_currentFrame++; //increment the frame count

	//and determine whether to cycle the animation
	if((_currentFrame / _refreshRate) >= _frameCount) {
		_currentFrame = 0;
	}
}

@end //end of implementation
