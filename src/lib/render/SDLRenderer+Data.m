/*
 * SDLRenderer+Data.m
 * Implements a category that allows access to an SDLRenderer's data field
 * Created by Andrew Davis
 * Created on 9/14/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the category header
#import "SDLRenderer+Data.h"

//category implementation
@implementation SDLRenderer (Data)

//data method - returns the SDLRenderer's data field
- (SDL_Renderer*) data {
	return _data; //return the data field
}

@end //end of implementation
