/*
 * SDLRenderer+Data.h
 * Declares a category that allows access to an SDLRenderer's data field
 * Created by Andrew Davis
 * Created on 9/14/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "SDLRenderer.h"
#import <SDL2/SDL.h>

//category declaration
@interface SDLRenderer (Data)

//returns the SDLRenderer's data field
- (SDL_Renderer*) data;

@end //end of header
