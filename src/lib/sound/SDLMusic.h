/*
 * SDLMusic.h
 * Declares a class that represents music
 * Created on 10/30/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <SDL2/SDL_mixer.h>

//class declaration
@interface SDLMusic : NSObject {
	//fields
	Mix_Music* _data; //the music data
	NSString* _musicPath; //the path to the current music file
}

//property declaration
@property (readonly) NSString* musicPath;

//method declarations

//initializes an SDLMusic instance
- (id) initWithPath: (NSString*) newMusicPath;

//deallocates an SDLMusic instance
- (void) dealloc;

//changes the target music file
- (void) loadMusicFromFile: (NSString*) filePath;

//plays the music
- (void) play;

//returns whether the music is playing
+ (bool) isPlaying;

//returns whether the music is paused
+ (bool) isPaused;

//pauses the music
+ (void) pause;

//stops the music
+ (void) stop;

@end //end of header
