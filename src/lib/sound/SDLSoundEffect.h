/*
 * SDLSoundEffect.h
 * Declares a class that represents a sound effect
 * Created on 10/30/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import <SDL2/SDL_mixer.h>

//class declaration
@interface SDLSoundEffect : NSObject {
	//fields
	Mix_Chunk* _data; //the sound data
	NSString* _soundPath; //the path to the sound file
}

//property declaration
@property (readonly) NSString* soundPath;

//method declarations

//initializes an SDLSoundEffect instance
- (id) initWithPath: (NSString*) newSoundPath;

//deallocates an SDLSoundEffect instance
- (void) dealloc;

//plays the sound effect once
- (void) play;

//plays the sound effect a variable number of times
- (void) playWithRepeat: (NSUInteger) times;

@end //end of header
