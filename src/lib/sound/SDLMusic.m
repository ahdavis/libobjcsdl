/*
 * SDLMusic.m
 * Implements a class that represents music
 * Created on 10/30/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLMusic.h"

//import the SDLSoundException class header
#import "../except/SDLSoundException.h"

//private method declarations
@interface SDLMusic ()

//frees the data fields
- (void) free;

@end 

//class implementation
@implementation SDLMusic

//property synthesis
@synthesize musicPath = _musicPath;

//init method - initializes an SDLMusic instance
- (id) initWithPath: (NSString*) newMusicPath {
	//call the superclass init method
	self = [super init];

	//make sure the call succeeded
	if(self) { //if the call succeeded
		//then load the music file
		_musicPath = nil;
		[self loadMusicFromFile: newMusicPath];
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLMusic instance
- (void) dealloc {
	[SDLMusic stop]; //stop the music
	[self free]; //free the data fields
	[super dealloc]; //and call the superclass dealloc method
}

//loadMusicFromFile method - loads music from a file
- (void) loadMusicFromFile: (NSString*) filePath {
	//save whether music was already playing
	bool wasPlaying = [SDLMusic isPlaying];

	//stop the music
	[SDLMusic stop];

	[self free]; //free the data fields

	//attempt to load the music
	_data = Mix_LoadMUS([filePath UTF8String]);

	//verify that the load succeeded
	if(_data == NULL) { //if the load failed
		//then throw an exception
		@throw [SDLSoundException exceptionWithPath: filePath];
	}

	//update the path field
	_musicPath = filePath;
	[_musicPath retain];

	//and restart the music (if it was already playing)
	if(wasPlaying) {
		[self play];
	}
}

//play method - plays the music
- (void) play {
	//play the music
	if(![SDLMusic isPlaying]) {
		Mix_PlayMusic(_data, -1);
	} else {
		if([SDLMusic isPaused]) {
			Mix_ResumeMusic();
		}
	}
}

//isPlaying class method - returns whether the music is playing
+ (bool) isPlaying {
	//return whether the music is playing
	return Mix_PlayingMusic() == 1; 
}

//isPaused class method - returns whether the music is paused
+ (bool) isPaused {
	//return whether the music is paused
	return Mix_PausedMusic() == 1;
}

//pause class method - pauses the music
+ (void) pause {
	Mix_PauseMusic(); //pause the music
}

//stop class method - stops the music
+ (void) stop {
	Mix_HaltMusic(); //stop the music
}

//private free method - frees the data fields
- (void) free {
	[_musicPath release]; //release the music path
	Mix_FreeMusic(_data); //free the data pointer
	_data = NULL; //and zero it out
}

@end //end of implementation
