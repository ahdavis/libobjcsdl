/*
 * SDLSoundEffect.m
 * Implements a class that represents a sound effect
 * Created on 10/30/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 * 
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLSoundEffect.h"

//import the SDLSoundException class header
#import "../except/SDLSoundException.h"

//class implementation
@implementation SDLSoundEffect

//property synthesis
@synthesize soundPath = _soundPath;

//init method - initializes an SDLSoundEffect instance
- (id) initWithPath: (NSString*) newSoundPath {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the data pointer
		_data = Mix_LoadWAV([newSoundPath UTF8String]);

		//verify that the initialization succeeded
		if(_data == NULL) { //if initialization failed
			//then throw an exception
			@throw [SDLSoundException 
				exceptionWithPath: newSoundPath];
		}

		//and init the path field
		_soundPath = newSoundPath;
		[_soundPath retain];
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLSoundEffect instance
- (void) dealloc {
	[_soundPath release]; //release the sound path
	Mix_FreeChunk(_data); //free the data pointer
	_data = NULL; //zero it out
	[super dealloc]; //and call the superclass dealloc method
}

//play method - plays the sound effect once
- (void) play {
	[self playWithRepeat: 0]; //call the other play method
}

//playWithRepeat method - plays the sound effect
//a variable number of times
- (void) playWithRepeat: (NSUInteger) times {
	Mix_PlayChannel(-1, _data, times); //play the effect
}

@end //end of implementation
