/*
 * SDLRect.h
 * Declares a class that represents a rectangle
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#include <Foundation/Foundation.h>
#import "SDLPoint.h"

//class declaration
@interface SDLRect : NSObject {
	//fields
	int _x; //the x-coord of the rectangle
	int _y; //the y-coord of the rectangle
	int _width; //the width of the rectangle
	int _height; //the height of the rectangle
}

//property declarations
@property (readwrite) int x;
@property (readwrite) int y;
@property (readwrite) int width;
@property (readwrite) int height;

//method declarations

//initializes an SDLRect instance at (0, 0) with a width and height of 1
- (id) init;

//initializes an SDLRect instance with programmer-defined dimensions
- (id) initWithX: (int) newX andY: (int) newY andWidth: (int) newWidth
	andHeight: (int) newHeight;

//deallocates an SDLRect instance
- (void) dealloc;

//returns whether the SDLRect collides with another
- (bool) collidesWith: (SDLRect*) other;

//returns whether an SDLPoint is inside the SDLRect
- (bool) containsPoint: (SDLPoint*) point;

@end //end of header
