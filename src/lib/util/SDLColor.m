/*
 * SDLColor.m
 * Implements a class that represents a color
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLColor.h"

//class implementation
@implementation SDLColor

//property synthesis
@synthesize red = _red;
@synthesize green = _green;
@synthesize blue = _blue;
@synthesize alpha = _alpha;

//first init method - initializes an SDLColor instance
//with an alpha value of 255 (0xFF)
- (id) initWithRed: (uint8_t) newRed andGreen: (uint8_t) newGreen
	andBlue: (uint8_t) newBlue {
	//call the second init method
	return [self initWithRed: newRed andGreen: newGreen
			 andBlue: newBlue andAlpha: 0xFF];
}

//second init method - initializes an SDLColor instance
//with a programmer-specified alpha value
- (id) initWithRed: (uint8_t) newRed andGreen: (uint8_t) newGreen
	andBlue: (uint8_t) newBlue andAlpha: (uint8_t) newAlpha {
	//call the superclass init method
	self = [super init];

	//verify that the call was successful
	if(self) { //if the call was successful
		//then init the fields
		_red = newRed;
		_green = newGreen;
		_blue = newBlue;
		_alpha = newAlpha;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLColor instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//first color class method - generates an autoreleased SDLColor instance
//with an alpha value of 255
+ (SDLColor*) colorWithRed: (uint8_t) newRed andGreen: (uint8_t) newGreen
	andBlue: (uint8_t) newBlue {
	//call the second color class method
	return [SDLColor colorWithRed: newRed andGreen: newGreen
			andBlue: newBlue andAlpha: 0xFF];
}

//second color class method - generates an autoreleased SDLColor instance
//with a programmer-specified alpha value
+ (SDLColor*) colorWithRed: (uint8_t) newRed andGreen: (uint8_t) newGreen
	andBlue: (uint8_t) newBlue andAlpha: (uint8_t) newAlpha {
	//return an autoreleased instance
	return [[[SDLColor alloc] initWithRed: newRed
			andGreen: newGreen andBlue: newBlue
				andAlpha: newAlpha]
				autorelease];
}

@end //end of implementation
