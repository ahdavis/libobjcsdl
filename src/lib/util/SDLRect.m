/*
 * SDLRect.m
 * Implements a class that represents a rectangle
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include the SDL header
#include <SDL2/SDL.h>

//import the class header
#import "SDLRect.h"

//import the SDLRect+Data category header
#import "SDLRect+Data.h"

//import the SDLPoint+Data category header
#import "SDLPoint+Data.h"

//class implementation
@implementation SDLRect

//property synthesis
@synthesize x = _x;
@synthesize y = _y;
@synthesize width = _width;
@synthesize height = _height;

//first init method - initializes an SDLRect instance at (0,0)
//with a width and height of 1
- (id) init {
	//call the second init method
	return [self initWithX: 0 andY: 0 andWidth: 1 andHeight: 1];
}

//second init method - initializes an SDLRect instance
//with programmer-defined dimensions
- (id) initWithX: (int) newX andY: (int) newY andWidth: (int) newWidth
	andHeight: (int) newHeight {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_x = newX;
		_y = newY;
		_width = newWidth;
		_height = newHeight;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLRect instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

//collidesWith method - returns whether an SDLRect collides with another
- (bool) collidesWith: (SDLRect*) other {
	//declare an SDL_Rect instance to pass to collision detection
	//without this, SDL_IntersectRect will always return false
	SDL_Rect dummy;

	//get an SDL_Rect instance from self
	SDL_Rect selfRect = [self data];

	//get an SDL_Rect instance from the other rect
	SDL_Rect otherRect = [other data];

	//determine whether the rectangles collide
	SDL_bool res = SDL_IntersectRect(&selfRect, &otherRect,
						&dummy);

	//and return the result, converted to an Objective-C boolean
	return res == SDL_TRUE;
}

//containsPoint method - returns whether an SDLPoint is inside
//the SDLRect
- (bool) containsPoint: (SDLPoint*) point {
	//load the point into an array
	SDL_Point points[] = {[point data]};

	//declare a dummy rect
	SDL_Rect dummy;

	//get an SDL_Rect instance from self
	SDL_Rect clip = [self data];

	//determine whether the point is inside the rectangle
	SDL_bool res = SDL_EnclosePoints(points, 1, &clip, &dummy);

	//and return the result, converted to an Objective-C boolean
	return res == SDL_TRUE;
}

@end //end of implementation
