/*
 * SDLPoint.h
 * Declares a class that represents a 2D point
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLPoint : NSObject {
	//fields
	int _x; //the x-component of the SDLPoint
	int _y; //the y-component of the SDLPoint
}

//property declarations
@property (readwrite) int x;
@property (readwrite) int y;

//method declarations

//initializes an SDLPoint at (0, 0)
- (id) init;

//initializes an SDLPoint at programmer-specified coordinates
- (id) initWithX: (int) newX andY: (int) newY;

//deallocates an SDLPoint instance
- (void) dealloc;

@end //end of header
