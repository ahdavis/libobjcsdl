/*
 * SDLColor+Data.m
 * Implements a category that allows conversion of an SDLColor instance
 * to an SDL_Color struct instance
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the category header
#import "SDLColor+Data.h"

//category implementation
@implementation SDLColor (Data)

//data method - converts the SDLColor to an SDL_Color
- (SDL_Color) data {
	//create an SDL_Color instance from the SDLColor
	SDL_Color ret;
	ret.r = _red;
	ret.g = _green;
	ret.b = _blue;
	ret.a = _alpha;

	//and return the instance
	return ret;
}

@end //end of implementation
