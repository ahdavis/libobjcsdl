/*
 * SDLClipboard.h
 * Declares a class that represents a clipboard
 * Created by Andrew Davis
 * Created on 2/5/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLClipboard : NSObject {
	//field
	NSString* _text; //the text stored on the clipboard
}

//property
@property (nonatomic, retain) NSString* text;

//method declarations

//returns an instance of the clipboard
+ (id) sharedClipboard;

//sets the text stored on the clipboard
- (void) setText: (NSString*) newText;

@end //end of class
