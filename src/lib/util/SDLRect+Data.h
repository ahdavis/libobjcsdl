/*
 * SDLRect+Data.h
 * Declares a category that allows the conversion of an SDLRect object
 * to an SDL_Rect structure instance
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#include <SDL2/SDL.h>
#import <Foundation/Foundation.h>
#import "SDLRect.h"

//category declaration
@interface SDLRect (Data)

//method declaration

//returns the SDL_Rect version of an SDLRect instance
- (SDL_Rect) data;

@end //end of header
