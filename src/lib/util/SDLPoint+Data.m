/*
 * SDLPoint+Data.m
 * Implements a category that allows for conversion of an SDLPoint object
 * to an SDL_Point struct instance
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the category header
#import "SDLPoint+Data.h"

//category implementation
@implementation SDLPoint (Data)

//data method - converts an SDLPoint object to an SDL_Point instance
- (SDL_Point) data {
	//declare the return value
	SDL_Point ret;

	//populate its fields
	ret.x = _x;
	ret.y = _y;

	//and return it
	return ret;
}

@end //end of implementation
