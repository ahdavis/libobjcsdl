/*
 * SDLRect+Data.m
 * Implements a category that allows the conversion of an SDLRect object
 * to an SDL_Rect structure instance
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the category header
#import "SDLRect+Data.h"

//category implementation
@implementation SDLRect (Data)

//data method - returns the SDLRect as an SDL_Rect instance
- (SDL_Rect) data {
	//generate an SDL_Rect instance
	SDL_Rect ret;
	ret.x = _x;
	ret.y = _y;
	ret.w = _width;
	ret.h = _height;

	//and return it
	return ret;
}

@end //end of implementation
