/*
 * SDLKeycode.h
 * Enumerates keycodes for libobjcsdl
 * Created by Andrew Davis
 * Created on 9/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include
#include <SDL2/SDL.h>

//enum definition
typedef enum {
	KEY_ZERO = SDLK_0, //zero key
	KEY_ONE = SDLK_1, //one key
	KEY_TWO = SDLK_2, //two key
	KEY_THREE = SDLK_3, //three key
	KEY_FOUR = SDLK_4, //four key
	KEY_FIVE = SDLK_5, //five key
	KEY_SIX = SDLK_6, //six key
	KEY_SEVEN = SDLK_7, //seven key
	KEY_EIGHT = SDLK_8, //eight key
	KEY_NINE = SDLK_9, //nine key
	KEY_A = SDLK_a, //letter A
	KEY_QUOTE = SDLK_QUOTE, //'
	KEY_B = SDLK_b, //letter B
	KEY_BACKSLASH = SDLK_BACKSLASH, //backslash
	KEY_BACKSPACE = SDLK_BACKSPACE, //backspace key
	KEY_C = SDLK_c, //letter C
	KEY_COMMA = SDLK_COMMA, //comma
	KEY_D = SDLK_d, //letter D
	KEY_DELETE = SDLK_DELETE, //delete key
	KEY_DOWN = SDLK_DOWN, //down key
	KEY_E = SDLK_e, //letter E
	KEY_EQUALS = SDLK_EQUALS, //equals sign
	KEY_ESC = SDLK_ESCAPE, //escape key
	KEY_F = SDLK_f, //letter F
	KEY_F1 = SDLK_F1, //F1 key
	KEY_F2 = SDLK_F2, //F2 key
	KEY_F3 = SDLK_F3, //F3 key
	KEY_F4 = SDLK_F4, //F4 key
	KEY_F5 = SDLK_F5, //F5 key
	KEY_F6 = SDLK_F6, //F6 key
	KEY_F7 = SDLK_F7, //F7 key
	KEY_F8 = SDLK_F8, //F8 key
	KEY_F9 = SDLK_F9, //F9 key
	KEY_F10 = SDLK_F10, //F10 key
	KEY_F11 = SDLK_F11, //F11 key
	KEY_F12 = SDLK_F12, //F12 key
	KEY_G = SDLK_g, //letter G
	KEY_BACKQUOTE = SDLK_BACKQUOTE, //`
	KEY_H = SDLK_h, //letter H
	KEY_I = SDLK_i, //letter I
	KEY_J = SDLK_j, //letter J
	KEY_K = SDLK_k, //letter K
	KEY_L = SDLK_l, //letter L
	KEY_LALT = SDLK_LALT, //left Alt key
	KEY_LCTRL = SDLK_LCTRL, //left Ctrl key
	KEY_LEFT = SDLK_LEFT, //left arrow key
	KEY_LBRACKET = SDLK_LEFTBRACKET, //[
	KEY_LGUI = SDLK_LGUI, //left Windows/Command/Meta key
	KEY_LSHIFT = SDLK_LSHIFT, //left Shift key
	KEY_M = SDLK_m, //letter M
	KEY_MINUS = SDLK_MINUS, //-
	KEY_N = SDLK_n, //letter N
	KEY_O = SDLK_o, //letter O
	KEY_P = SDLK_p, //letter P
	KEY_PERIOD = SDLK_PERIOD, //.
	KEY_Q = SDLK_q, //letter Q
	KEY_R = SDLK_r, //letter R
	KEY_RALT = SDLK_RALT, //right Alt key
	KEY_RCTRL = SDLK_RCTRL, //right Ctrl key
	KEY_RETURN = SDLK_RETURN, //Enter key
	KEY_RGUI = SDLK_RGUI, //right Windows/Command/Meta key
	KEY_RIGHT = SDLK_RIGHT, //right arrow key
	KEY_RIGHTBRACKET = SDLK_RIGHTBRACKET, //]
	KEY_RSHIFT = SDLK_RSHIFT, //right Shift key
	KEY_S = SDLK_s, //letter S
	KEY_SEMICOLON = SDLK_SEMICOLON, //;
	KEY_FORWARDSLASH = SDLK_SLASH, //forward slash
	KEY_SPACEBAR = SDLK_SPACE, //spacebar
	KEY_T = SDLK_t, //letter T
	KEY_TAB = SDLK_TAB, //Tab key
	KEY_U = SDLK_u, //letter Y
	KEY_UP = SDLK_UP, //up arrow key
	KEY_V = SDLK_v, //letter V
	KEY_W = SDLK_w, //letter W
	KEY_X = SDLK_x, //letter X
	KEY_Y = SDLK_y, //letter Y
	KEY_Z = SDLK_z, //letter Z
	KEY_AMPERSAND = SDLK_AMPERSAND, //&
	KEY_ASTERISK = SDLK_ASTERISK, //*
	KEY_AT = SDLK_AT, //@
	KEY_CARET = SDLK_CARET, //^
	KEY_COLON = SDLK_COLON, //:
	KEY_DOLLAR = SDLK_DOLLAR, //$
	KEY_EXCLAIM = SDLK_EXCLAIM, //!
	KEY_GREATERTHAN = SDLK_GREATER, //>
	KEY_HASH = SDLK_HASH, //#
	KEY_LPAREN = SDLK_LEFTPAREN, //(
	KEY_LESSTHAN = SDLK_LESS, //<
	KEY_PERCENT = SDLK_PERCENT, //%
	KEY_PLUS = SDLK_PLUS, //+
	KEY_QUESTION = SDLK_QUESTION, //?
	KEY_DBLQUOTE = SDLK_QUOTEDBL, //"
	KEY_RPAREN = SDLK_RIGHTPAREN, //)
	KEY_UNDERSCORE = SDLK_UNDERSCORE //_
} SDLKeycode;

//end of header
