/*
 * SDLPoint.m
 * Implements a class that represents a 2D point
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLPoint.h"

//class implementation
@implementation SDLPoint

//property synthesis
@synthesize x = _x;
@synthesize y = _y;

//first init method - initializes an SDLPoint at (0, 0)
- (id) init {
	//call the second init method
	return [self initWithX: 0 andY: 0];
}

//second init method - initializes an SDLPoint at
//programmer-specified coordinates
- (id) initWithX: (int) newX andY: (int) newY {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_x = newX;
		_y = newY;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLPoint instance
- (void) dealloc {
	[super dealloc]; //call the superclass dealloc method
}

@end //end of implementation
