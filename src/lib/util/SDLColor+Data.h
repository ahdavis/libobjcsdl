/*
 * SDLColor+Data.h
 * Declares a category that allows conversion of an SDLColor instance
 * to an SDL_Color struct instance
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#include <SDL2/SDL.h>
#import <Foundation/Foundation.h>
#import "SDLColor.h"

//category declaration
@interface SDLColor (Data)

//method declaration

//converts the SDLColor to an SDL_Color
- (SDL_Color) data;

@end //end of header
