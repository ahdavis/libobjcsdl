/*
 * SDLPoint+Data.h
 * Declares a category that allows for conversion of an SDLPoint object
 * to an SDL_Point struct instance
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#include <SDL2/SDL.h>
#import <Foundation/Foundation.h>
#import "SDLPoint.h"

//category declaration
@interface SDLPoint (Data)

//method declaration

//converts an SDLPoint object to an SDL_Point instance
- (SDL_Point) data;

@end //end of header
