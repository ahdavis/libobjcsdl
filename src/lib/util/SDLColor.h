/*
 * SDLColor.h
 * Declares a class that represents a color
 * Created by Andrew Davis
 * Created on 9/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface SDLColor : NSObject {
	//fields
	uint8_t _red; //the red component of the color
	uint8_t _green; //the green component of the color
	uint8_t _blue; //the blue component of the color
	uint8_t _alpha; //the alpha component of the color
}

//property declarations
@property (readwrite) uint8_t red;
@property (readwrite) uint8_t green;
@property (readwrite) uint8_t blue;
@property (readwrite) uint8_t alpha;

//method declarations

//initializes an SDLColor instance with an alpha value of 255 (0xFF)
- (id) initWithRed: (uint8_t) newRed andGreen: (uint8_t) newGreen
	andBlue: (uint8_t) newBlue;

//initializes an SDLColor instance with a programmer-specified alpha value
- (id) initWithRed: (uint8_t) newRed andGreen: (uint8_t) newGreen
	andBlue: (uint8_t) newBlue andAlpha: (uint8_t) newAlpha;

//deallocates an SDLColor instance
- (void) dealloc;

//returns an autoreleased SDLColor instance with an alpha value of 255
+ (SDLColor*) colorWithRed: (uint8_t) newRed andGreen: (uint8_t) newGreen
	andBlue: (uint8_t) newBlue;

//returns an autoreleased SDLColor instance with 
//a programmer-specified alpha value
+ (SDLColor*) colorWithRed: (uint8_t) newRed andGreen: (uint8_t) newGreen
	andBlue: (uint8_t) newBlue andAlpha: (uint8_t) newAlpha;

@end //end of header
