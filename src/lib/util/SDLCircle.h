/*
 * SDLCircle.h
 * Declares a class that represents a circle
 * Created by Andrew Davis
 * Created on 9/13/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "SDLPoint.h"

//class declaration
@interface SDLCircle : NSObject {
	//fields
	SDLPoint* _origin; //the origin point of the circle
	int _radius; //the radius of the circle
}

//property declarations
@property (retain) SDLPoint* origin; 
@property (readwrite) int radius;

//method declarations

//initializes an SDLCircle instance with an origin of (0, 0)
//and a radius of 1
- (id) init;

//initializes an SDLCircle instance with a programmer-specified 
//origin and radius
- (id) initWithXPos: (int) xPos andYPos: (int) yPos 
	andRadius: (int) newRadius;

//initializes an SDLCircle instance with an origin specified by
//an SDLPoint object
- (id) initWithOrigin: (SDLPoint*) newOrigin andRadius: (int) newRadius;

//deallocates an SDLCircle instance
- (void) dealloc;

//returns whether a point is inside the circle
- (bool) containsPoint: (SDLPoint*) point;

@end //end of header
