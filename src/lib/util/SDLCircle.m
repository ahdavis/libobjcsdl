/*
 * SDLCircle.m
 * Implements a class that represents a circle
 * Created by Andrew Davis
 * Created on 9/13/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLCircle.h"

//import the math header
#import <math.h>

//class implementation
@implementation SDLCircle

//property synthesis
@synthesize origin = _origin;
@synthesize radius = _radius;

//first init method - initializes an SDLCircle instance at (0, 0)
//with a radius of 1
- (id) init {
	//call the second init method
	return [self initWithXPos: 0 andYPos: 0 andRadius: 1];
}

//second init method - initializes an SDLCircle instance
//with a programmer-specified origin and radius
- (id) initWithXPos: (int) xPos andYPos: (int) yPos
	andRadius: (int) newRadius {
	//call the third init method
	return [self initWithOrigin: 
		[[SDLPoint alloc] initWithX: xPos andY: yPos]
				  andRadius: newRadius];
}

//third init method - initializes an SDLCircle instance
//with an origin specified by an SDLPoint object
- (id) initWithOrigin: (SDLPoint*) newOrigin andRadius: (int) newRadius {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_origin = newOrigin;
		[_origin retain];
		_radius = newRadius;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates an SDLCircle instance
- (void) dealloc {
	[_origin release]; //release the origin field
	[super dealloc]; //and call the superclass dealloc method
}

//containsPoint method - returns whether a point is inside the circle
- (bool) containsPoint: (SDLPoint*) point {
	//get the squared distance between the point and the origin
	double xDist = pow([point x] - [_origin x], 2);
	double yDist = pow([point y] - [_origin y], 2);
	double distSquared = xDist + yDist;

	//compare the distance with the radius of the circle
	if(sqrt(distSquared) > _radius) { //if the point is outside
		return NO; //then return false
	} else { //if the point is inside
		return YES; //then return true
	}
}

@end //end of implementation
