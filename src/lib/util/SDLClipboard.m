/*
 * SDLClipboard.m
 * Implements a class that represents a clipboard
 * Created by Andrew Davis
 * Created on 2/5/2019
 *
 * Copyright 2019 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//import the class header
#import "SDLClipboard.h"

//import the SDL header
#include <SDL2/SDL.h>

//singleton instance variable
static SDLClipboard* sharedInstance = nil;

//class implementation
@implementation SDLClipboard

//property synthesis
@synthesize text = _text;

//sharedClipboard class method - returns an instance of the clipboard
+ (id) sharedClipboard {
	//initialize the singleton
	@synchronized(self) {
		if(sharedInstance == nil) {
			sharedInstance = 
				[[super allocWithZone: NULL] init];
		}
	}

	//and return the instance
	return sharedInstance;
}

//setText method - sets the text of the clipboard
- (void) setText: (NSString*) newText {
	//update the text field
	[_text release];
	_text = newText;
	[_text retain];

	//and set the clipboard text
	SDL_SetClipboardText([_text UTF8String]);
}

//private method implementations

//specialized allocation method
+ (id) allocWithZone: (NSZone*) zone {
	return [[self sharedClipboard] retain];
}

//specialized copy method
- (id) copyWithZone: (NSZone*) zone {
	return self;
}

//retains the clipboard
- (id) retain {
	return self;
}

//returns the clipboard's retain count
- (NSUInteger) retainCount {
	return UINT_MAX;
}

//releases the clipboard
- (oneway void) release {
	//never release
}

//autoreleases the clipboard
- (id) autorelease {
	return self;
}

//initializes the clipboard
- (id) init {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the field
		_text = [[NSString alloc] 
				initWithCString: SDL_GetClipboardText()];
	}

	//and return the instance
	return self;
}

//deallocates the clipboard
- (void) dealloc {
	[_text release]; //release the text
	[super dealloc]; //and call the superclass dealloc method
}

@end //end of implementation
