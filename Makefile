# Makefile for libobjcsdl
# Compiles the code for the library
# Created on 9/8/2018
# Created by Andrew Davis
#
# Copyright (C) 2018  Andrew Davis
#
# Licensed under the Lesser GNU General Public License, version 3

# define the compiler
CC=gcc

# define the compiler flags for the library
CFLAGS=`gnustep-config --objc-flags` -x objective-c -fPIC -c -Wall
CFLAGS += -Wno-unused-but-set-variable

# define the compiler flags for the test program
test: CFLAGS=`gnustep-config --objc-flags` -x objective-c -c -Wall -g

# define linker flags for the library
LDFLAGS=-shared -lSDL2 -lSDL2_image -lgnustep-base -lobjc 
LDFLAGS += -lSDL2_ttf -lSDL2_mixer -lm

# define linker flags for the test program
TFLAGS=-lgnustep-base -lobjc -L./lib -Wl,-rpath,./lib -lobjcsdl

# retrieve source code for the library
LCTX=$(shell ls src/lib/context/*.m)
LEXCE=$(shell ls src/lib/except/*.m)
LWIN=$(shell ls src/lib/window/*.m)
LUTIL=$(shell ls src/lib/util/*.m)
LELEM=$(shell ls src/lib/element/*.m)
LEVNT=$(shell ls src/lib/event/*.m)
LREND=$(shell ls src/lib/render/*.m)
LGUI=$(shell ls src/lib/gui/*.m)
LSND=$(shell ls src/lib/sound/*.m)
LTIME=$(shell ls src/lib/time/*.m)
LTHRD=$(shell ls src/lib/thread/*.m)

# list the source code for the library
LSOURCES=$(LCTX) $(LEXCE) $(LWIN) $(LUTIL) $(LELEM) $(LEVNT) $(LREND)
LSOURCES += $(LGUI) $(LSND) $(LTIME) $(LTHRD)

# compile the source code for the library
LOBJECTS=$(LSOURCES:.m=.o)

# retrieve source code for the test program
TMAIN=$(shell ls src/test/*.m)

# list the source code for the test program
TSOURCES=$(TMAIN)

# compile the source code for the test program
TOBJECTS=$(TSOURCES:.m=.o)

# define the name of the library
LIB=libobjcsdl.so

# define the name of the test executable
TEST=demo

# rule for building both the library and the test program
all: library test

# master rule for compiling the library
library: $(LSOURCES) $(LIB)

# master rule for compiling the test program
test: $(TSOURCES) $(TEST)

# sub-rule for compiling the library
$(LIB): $(LOBJECTS)
	$(CC) $(LOBJECTS) -o $@ $(LDFLAGS)
	mkdir lib
	mkdir lobj
	mv -f $(LOBJECTS) lobj/
	mv -f $@ lib/
	find . -type f -name '*.d' -delete

# sub-rule for compiling the test program
$(TEST): $(TOBJECTS)
	$(CC) $(TOBJECTS) -o $@ $(TFLAGS)
	mkdir bin
	mkdir tobj
	mv -f $(TOBJECTS) tobj/
	mv -f $@ bin/
	find . -type f -name '*.d' -delete

# rule for compiling source code to object code
.m.o:
	$(CC) $(CFLAGS) $< -o $@

# target to install the compiled library
# REQUIRES ROOT
install:
	if [ -f "/usr/lib/libobjcsdl.so" ]; then \
		rm /usr/lib/libobjcsdl.so; \
	fi 
	if [ -d "/usr/include/objcsdl" ]; then \
		rm -rf /usr/include/objcsdl; \
	fi 
	
	mkdir /usr/include/objcsdl
	mkdir /usr/include/objcsdl/context 
	mkdir /usr/include/objcsdl/window 
	mkdir /usr/include/objcsdl/except 
	mkdir /usr/include/objcsdl/util 
	mkdir /usr/include/objcsdl/element 
	mkdir /usr/include/objcsdl/event 
	mkdir /usr/include/objcsdl/render 
	mkdir /usr/include/objcsdl/gui 
	mkdir /usr/include/objcsdl/sound 
	mkdir /usr/include/objcsdl/time 
	mkdir /usr/include/objcsdl/thread 
	cp src/lib/objcsdl.h /usr/include/objcsdl/
	cp $(shell ls src/lib/context/*.h) /usr/include/objcsdl/context/
	cp $(shell ls src/lib/window/*.h) /usr/include/objcsdl/window/
	cp $(shell ls src/lib/except/*.h) /usr/include/objcsdl/except/
	cp $(shell ls src/lib/util/*.h) /usr/include/objcsdl/util/
	cp $(shell ls src/lib/element/*.h) /usr/include/objcsdl/element/
	cp $(shell ls src/lib/event/*.h) /usr/include/objcsdl/event/
	cp $(shell ls src/lib/render/*.h) /usr/include/objcsdl/render/
	cp $(shell ls src/lib/gui/*.h) /usr/include/objcsdl/gui/
	cp $(shell ls src/lib/sound/*.h) /usr/include/objcsdl/sound/
	cp $(shell ls src/lib/time/*.h) /usr/include/objcsdl/time/
	cp $(shell ls src/lib/thread/*.h) /usr/include/objcsdl/thread/
	cp ./lib/$(LIB) /usr/lib/

# target to clean the workspace
clean:
	if [ -d "lobj" ]; then \
		rm -rf lobj; \
	fi
	if [ -d "lib" ]; then \
		rm -rf lib; \
	fi
	if [ -d "bin" ]; then \
		rm -rf bin; \
	fi
	if [ -d "tobj" ]; then \
		rm -rf tobj; \
	fi

# end of Makefile
